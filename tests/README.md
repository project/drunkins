I'm only beginning to write tests.

Unless I'm missing something, unit tests are not very practical here.
If the 'runners' were actually classes with a defined interface, unit tests
could be written for those.

Functional tests are absolutely necessary, but I'm not looking forward to
writing any functional tests that mimic cron or the batch API, in order to
test jobs on those.

Luckily, we have drush support for this module now. So I'm starting to write
something like functional tests, that will hopefully aid in not introducing
bugs during the rewrite of various interfaces.

Proper drush tests are written in Unish like the example at
http://drupalsun.com/ben/2014/11/28/getting-started-testing-drush-commands
but so far there is only one test, which I haven't automated yet; that's the
next step. For now, testing can be done manually on an existing website by:

$ drush en drunkins_test
$ drush drunkins-run-job <testname> # e.g. drunkins_restart_test_1
$ drush vget drunkins_test_result

See drunkins_test.module for the expected values of the variable.

We can also use PHPUnit to test a job's code by calling start() + processItem()
by ourselves. That could be better for more extensively testing some jobs'
internal logic - but does not emulate complete process runs. See
https://github.com/rmuit/copernica-api/blob/master/extra/CopernicaUpdateProfilesJobTest.php
for an example.
