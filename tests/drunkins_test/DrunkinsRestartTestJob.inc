<?php

/**
 * Job to test behavior or runners when start() gets called multiple times.
 */
class DrunkinsRestartTestJob implements DrunkinsJobInterface {

  /**
   * Settings.
   *
   * Of importance to this test class:
   * - batch_item_count (int): the number of items to return from start().
   * - batch_count (int, default 1): the number of batches to return (i.e. the
   *   number of times start() is called repeatedly before starting to process
   *   items).
   * - finish_refetch_count (int, default 0): fetch another batch with more items
   *   on finish N times, i.e. the job calls start() in total <batch_count +
   *   finish_refetch_count>.
   * - batch_item_count_2, batch_item_count_3, etc: the number of items to
   *   return on the 2nd, 3rd etc. batch, if different from batch_item_count.
   *   (batch_item_count_1 has no meaning. Note that if the job fetches more
   *   items at finish, the batch counter will keep going up.)
   * - 'throw' (array): the run numbers at which a non-temporary exception
   *   should be thrown.
   * - 'throw_temp_error' (array): the run numbers at which a temporary error
   *   should be thrown.
   *
   * @see Drunkinsjob::$settings
   *
   * @var array
   */
  protected $settings;

  /**
   * Constructor.
   *
   * @param array $settings
   *   The initial process settings. See property definition (or comments at the
   *   top of extending classes, hopefully) for supported keys.
   */
  public function __construct(array $settings = array()) {
    $this->settings = $settings;
  }

  /**
   * Implements DrunkinsFetcherInterface::start().
   */
  public function start(array &$context) {
    $run_count = empty($context['drunkins_rerun_start']) ? 1 : ($context['drunkins_rerun_start'] + 1);

    // Get descriptive part for 'name' property, and total already fetched,
    // in case of multiple batches,
    $subtotal_fetched = 0;
    $suffixes = [];
    $batch_count_suffix = '';
    if (!empty($context['drunkins_rerun_start'])) {
      // We also have the 'drunkins_rerun_start_count' context value, but that
      // one does not have a correct value at finish().
      if (!isset($context['test_total_fetched']) || !is_int($context['test_total_fetched']) || $context['test_total_fetched'] < 0) {
        throw new RuntimeException("'test_total_fetched' context has no correct value; the test may need to be rewritten.");
      }
      if ($context['drunkins_rerun_start_count'] !== $context['test_total_fetched']) {
        // This is not even important for anything. I just want to keep tabs on
        // it and reevaluate whether I'm till doing the right thing, if
        // 'drunkins_rerun_start_count' is ever not set.
        throw new RuntimeException("'drunkins_rerun_start_count' context has no correct value; the test may need to be rewritten.");
      }
      $subtotal_fetched = $context['test_total_fetched'];

      if (!is_int($context['drunkins_rerun_start']) || $context['drunkins_rerun_start'] <= 0) {
        throw new RuntimeException("'drunkins_rerun_start' context has an illegal value.");
      }
      $suffixes[] = "batch $run_count";
      $batch_count_suffix = "_$run_count";
    }
    if (!empty($context['test_rerun'])) {
      $suffixes[] = "re-run $context[test_rerun]";
    }
    $suffix = $suffixes ? ' (' . implode(', ', $suffixes) . ')' : '';

    if (!empty($this->settings['throw_temp_error'])) {
      if (!is_array($this->settings['throw_temp_error'])) {
        throw new RuntimeException("'throw_temp_error' setting must be an array.");
      }
      if (in_array($run_count, $this->settings['throw_temp_error'])) {
        throw new DrunkinsTemporaryError("Simulating temporary error at run $run_count.");
      }
    }
    if (!empty($this->settings['throw'])) {
      if (!is_array($this->settings['throw'])) {
        throw new RuntimeException("'throw' setting must be an array.");
      }
      if (in_array($run_count, $this->settings['throw'])) {
        throw new RuntimeException("Simulating exception at run $run_count.");
      }
    }

    // Get count of items to add in this batch.
    if (isset($this->settings["batch_item_count$batch_count_suffix"])) {
      $item_count_for_this_batch = $this->settings["batch_item_count$batch_count_suffix"];
      if (filter_var($item_count_for_this_batch, FILTER_VALIDATE_INT, ['options' => ['min_range' => 0]]) === FALSE) {
        throw new RuntimeException("'batch_item_count$batch_count_suffix' setting is not a positive integer.");
      }
    }
    elseif (!isset($this->settings['batch_item_count'])) {
      throw new RuntimeException("'batch_item_count' setting not defined.");
    }
    else {
      // This does not need checking anymore because we've gone through the
      // if() on the first iteration.
      $item_count_for_this_batch = $this->settings['batch_item_count'];
    }

    // We want to keep incrementing the ID over multiple batches (regardless
    // whether start() was called again immediately or after processing items),
    // so we'll remember the total item count.

    // This should be fairly straightforward because both 'drunkins_rerun_start'
    // and 'drunkins_rerun_start_count' increase and never get reset. We will
    // take 'drunkins_rerun_start_count' as the basis for the ID; if it ever
    // gets impractical to require this number to always be remembered between
    // runs, we could reevaluate. (The number is not guaranteed OK when a job
    // allows enqueueing items by a fetcher. Which this job does not.)
    $items = [];
    $max = $subtotal_fetched + $item_count_for_this_batch;
    while ($subtotal_fetched < $max) {
      // We'll make this 1-based, not 0-based; increading before 'fetching'.
      $subtotal_fetched++;

      $items[] = [
        'id' => $subtotal_fetched,
        'name' => "Item $subtotal_fetched$suffix",
      ];
    }
    $context['test_total_fetched'] = $subtotal_fetched;

    // Check if we need to 'fetch' another batch after this.
    if (empty($context['test_rerunning_from_finish']) && isset($this->settings['batch_count'])) {
      $total_batches_to_fetch = $this->settings['batch_count'];
      if (filter_var($total_batches_to_fetch, FILTER_VALIDATE_INT, ['options' => ['min_range' => 1]]) === FALSE) {
        throw new RuntimeException("'batch_count' setting is not a positive integer.");
      }

      if ($total_batches_to_fetch != 1) {
        $batches_fetched = (!empty($context['drunkins_rerun_start']) ? $context['drunkins_rerun_start'] : 0) + 1;
        if ($batches_fetched < $total_batches_to_fetch) {
          $context['drunkins_process_more'] = TRUE;
        }
      }
    }

    return $items;
  }

  /**
   * Implements DrunkinsJobInterface::processItem().
   */
  public function processItem($item, array &$context) {
    // We store all the items in a context sub-array and will take care of the
    // full value on finish().
    $context['test_result'][] = $item;
  }

  /**
   * Implements DrunkinsFetcherInterface::finish().
   */
  public function finish(array &$context) {
    if (!empty($this->settings['finish_refetch_count'])) {
      if (filter_var($this->settings['finish_refetch_count'], FILTER_VALIDATE_INT, ['options' => ['min_range' => 0]]) === FALSE) {
        throw new RuntimeException("'finish_refetch_count' setting is not a positive integer.");
      }

      // I might have used 'drunkins_progress' instead but I didn't think of
      // that when writing the test - and I'm not sure if it's allowed either.
      // @todo maybe check if I want/need to check drunkins_progress later. Maybe.
      if (empty($context['test_total_fetched']) || !is_int($context['test_total_fetched']) || $context['test_total_fetched'] <= 0) {
        throw new RuntimeException("'test_total_fetched' context has no correct value on finish.");
      }

      // There is no law governing the value of drunkins_rerun_start/count here,
      // but we happen to be using them for consistent numbering. If they ever
      // go empty despite start() already having been run multiple times, we'll
      // need to rewrite the code that is now start(), to keep different
      // counters.
      if (isset($context['drunkins_rerun_start'])) {
        if (!is_int($context['drunkins_rerun_start']) || $context['drunkins_rerun_start'] <= 0) {
          throw new RuntimeException("'drunkins_rerun_start' context has no correct value on finish; the test may need to be rewritten.");
        }
        if ($context['drunkins_rerun_start'] == 1) {
          // The value is either not set, or set right after it was discovered
          // that we needed to rerun start() AND increased after every call to
          // start(), to be equal to the times start() was run already, i.e.
          // now minimum 2.
          throw new RuntimeException("'drunkins_rerun_start' context is set to 1 on finish; this must never happen (so there is a programming error in the runner code) or our test assumption is totally off.");
        }
      }
      else {
        // Our start() needs this:
        $context['drunkins_rerun_start'] = 1;
        $context['drunkins_rerun_start_count'] = $context['test_total_fetched'];
      }

      $total_batches = $this->settings['finish_refetch_count'] +
                       (isset($this->settings['batch_count']) ? $this->settings['batch_count'] : 1);
      if ($context['drunkins_rerun_start'] < $total_batches) {
        // We're not done yet; get more items and return them in 'process_more'.

        // Our start() also wants to know the 're-run' count. We can measure
        // this in a couple of ways, we'll do a separate counter which we
        // increase each time we run start() from here.
        $context['test_rerun'] = (isset($context['test_rerun']) ? $context['test_rerun'] : 0) + 1;

        if (!empty($context['drunkins_process_more'])) {
          // This is a doublecheck; if this is nonempty, the fetcher is broken.
          throw new RuntimeException("'drunkins_process_more' context is nonempty at finish(). There must be a programming error in the runner code.");
        }
        $items = $this->start($context);
        if (!empty($context['drunkins_process_more'])) {
          // start() should only ever set this when deciding to re-fetch items at
          // the start of the job; this must be our own fault.
          throw new RuntimeException("'drunkins_process_more' context is set nonempty by start() called from finish(). There must be a programming error in the test job class.");
        }
        $context['drunkins_process_more'] = $items;

        $context['drunkins_rerun_start']++;
        // This is just so we keep adhering to our own check in case we refetch
        // again later:
        $context['drunkins_rerun_start_count'] += count($items);
      }
    }

    if (empty($context['drunkins_process_more'])) {
      // Save our result somewhere.
      variable_set('drunkins_test_result', ['time' => time(), 'result' => $context['test_result']]);
      $this->log('Job context : @context', array('@context' => json_encode($context)), WATCHDOG_DEBUG);
    }
  }

  /**
   * Logs or prints a message. Straight copy from job.inc.
   *
   * @param string $message
   *   Untranslated message, optionally containing placeholders for translation.
   * @param array $variables
   *   (optional) Replacements to make after translation.
   * @param int $severity
   *   (optional) Severity constant; see watchdog_severity_levels().
   * @param bool $repeat
   *   (optional) If this is FALSE and the message is already set, then the
   *   message won't be repeated.
   *
   * @see t()
   * @see watchdog_severity_levels()
   *
   * @todo this method does not have access to a 'log threshold' setting
   *   (unlike _drunkins_log() which has the runner setting 'log_level', which
   *   should be renamed to 'log_threshold' but which might also disappear when
   *   we switch to PSR-3 logging? Haven't decided yet whether this wrapper
   *   method should reference a per-job threshold setting.)
   */
  public function log($message, array $variables = array(), $severity = WATCHDOG_NOTICE, $repeat = TRUE) {
    // Until we use a PSR-3 logger: reuse code by calling a global function.
    $job_id = isset($this->settings['job_id']) ? $this->settings['job_id'] : '';
    if (!isset($repeat)) {
      // Bad code which will confuse __drunkins_log(). NULL would behave the
      // same as FALSE so use that.
      $repeat = FALSE;
    }
    __drunkins_log($job_id, $message, $variables, $severity, $this->settings, $repeat);
  }

}
