<?php
/**
 * @file
 * Default Views definition(s) for.
 */

/**
 * Implements hook_views_default_views().
 */
function drunkins_views_default_views() {
  $view = new view();
  $view->name = 'drunkins_process_counts';
  $view->description = '';
  $view->tag = '';
  $view->base_table = 'drunkins_process_run';
  $view->human_name = 'Drunkins process statistics';
  $view->core = 0;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Default */
  $handler = $view->new_display('default', 'Default', 'default');
  $handler->display->display_options['title'] = 'Drunkins process statistics';
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['access']['perm'] = 'access drunkins advanced';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'full';
  $handler->display->display_options['pager']['options']['items_per_page'] = '100';
  $handler->display->display_options['pager']['options']['offset'] = '0';
  $handler->display->display_options['pager']['options']['id'] = '0';
  $handler->display->display_options['pager']['options']['quantity'] = '9';
  $handler->display->display_options['style_plugin'] = 'views_crosstab_table';
  $handler->display->display_options['style_options']['columns'] = array(
    'id' => 'id',
    'pid' => 'pid',
    'name_1' => 'name_1',
    'started' => 'started',
    'finished' => 'finished',
    'name' => 'name',
    'count' => 'count',
  );
  $handler->display->display_options['style_options']['class'] = '';
  $handler->display->display_options['style_options']['default'] = 'started';
  $handler->display->display_options['style_options']['info'] = array(
    'id' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'pid' => array(
      'sortable' => 1,
      'default_sort_order' => 'desc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'name_1' => array(
      'sortable' => 1,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'started' => array(
      'sortable' => 1,
      'default_sort_order' => 'desc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'finished' => array(
      'sortable' => 1,
      'default_sort_order' => 'desc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'name' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'count' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
  );
  $handler->display->display_options['style_options']['crosstab_rows'] = 'id';
  $handler->display->display_options['style_options']['crosstab_columns'] = 'name';
  $handler->display->display_options['style_options']['crosstab_data'] = 'count';
  $handler->display->display_options['style_options']['crosstab_operation'] = 'MAX';
  $handler->display->display_options['style_options']['include_crosstab_operation_on_row'] = FALSE;
  $handler->display->display_options['style_options']['include_crosstab_operation_on_column'] = FALSE;
  /* Header: Global: Text area */
  $handler->display->display_options['header']['area']['id'] = 'area';
  $handler->display->display_options['header']['area']['table'] = 'views';
  $handler->display->display_options['header']['area']['field'] = 'area';
  $handler->display->display_options['header']['area']['label'] = 'Help';
  $handler->display->display_options['header']['area']['content'] = 'There\'s no consistent meaning behind the various counter values; every job can define what they mean - except \'drunkins_queued\' means how many items were fetched <em>and</em> queued for later processing. So their main uses are:
  <ul>
  <li>getting an idea about queue usage;</li>
  <li>comparing the same counter values for the same job over time;</li>
  <li>judging the values by someone who knows exactly what they mean for a certain job.</li>
  </ul>';
  $handler->display->display_options['header']['area']['format'] = 'basic_html';
  /* No results behavior: Global: Text area */
  $handler->display->display_options['empty']['text']['id'] = 'text';
  $handler->display->display_options['empty']['text']['table'] = 'views';
  $handler->display->display_options['empty']['text']['field'] = 'area';
  $handler->display->display_options['empty']['text']['empty'] = TRUE;
  $handler->display->display_options['empty']['text']['content'] = 'There is no data in this table. "Save process counts" should likely be enabled for that to happen.';
  $handler->display->display_options['empty']['text']['format'] = 'plain_text';
  /* Field: Drunkins process: ID */
  $handler->display->display_options['fields']['id']['id'] = 'id';
  $handler->display->display_options['fields']['id']['table'] = 'drunkins_process_run';
  $handler->display->display_options['fields']['id']['field'] = 'id';
  $handler->display->display_options['fields']['id']['separator'] = '';
  /* Field: Drunkins process: (Queue or job) name */
  $handler->display->display_options['fields']['name_1']['id'] = 'name_1';
  $handler->display->display_options['fields']['name_1']['table'] = 'drunkins_process_run';
  $handler->display->display_options['fields']['name_1']['field'] = 'name';
  $handler->display->display_options['fields']['name_1']['label'] = 'Job name';
  /* Field: Drunkins process: Started */
  $handler->display->display_options['fields']['started']['id'] = 'started';
  $handler->display->display_options['fields']['started']['table'] = 'drunkins_process_run';
  $handler->display->display_options['fields']['started']['field'] = 'started';
  /* Field: Drunkins process: Finished */
  $handler->display->display_options['fields']['finished']['id'] = 'finished';
  $handler->display->display_options['fields']['finished']['table'] = 'drunkins_process_run';
  $handler->display->display_options['fields']['finished']['field'] = 'finished';
  /* Field: Drunkins process counter: Counter name */
  $handler->display->display_options['fields']['name']['id'] = 'name';
  $handler->display->display_options['fields']['name']['table'] = 'drunkins_process_counter';
  $handler->display->display_options['fields']['name']['field'] = 'name';
  /* Field: Drunkins process counter: Count */
  $handler->display->display_options['fields']['count']['id'] = 'count';
  $handler->display->display_options['fields']['count']['table'] = 'drunkins_process_counter';
  $handler->display->display_options['fields']['count']['field'] = 'count';
  /* Filter criterion: Drunkins process: Parent ID */
  $handler->display->display_options['filters']['pid']['id'] = 'pid';
  $handler->display->display_options['filters']['pid']['table'] = 'drunkins_process_run';
  $handler->display->display_options['filters']['pid']['field'] = 'pid';
  $handler->display->display_options['filters']['pid']['operator'] = 'empty';
  $handler->display->display_options['filters']['pid']['group'] = 1;
  $handler->display->display_options['filters']['pid']['expose']['operator_id'] = 'pid_op';
  $handler->display->display_options['filters']['pid']['expose']['label'] = 'Parent ID';
  $handler->display->display_options['filters']['pid']['expose']['operator'] = 'pid_op';
  $handler->display->display_options['filters']['pid']['expose']['identifier'] = 'pid';
  /* Filter criterion: Drunkins process: (Queue or job) name */
  $handler->display->display_options['filters']['name']['id'] = 'name';
  $handler->display->display_options['filters']['name']['table'] = 'drunkins_process_run';
  $handler->display->display_options['filters']['name']['field'] = 'name';
  $handler->display->display_options['filters']['name']['operator'] = 'starts';
  $handler->display->display_options['filters']['name']['group'] = 1;
  $handler->display->display_options['filters']['name']['exposed'] = TRUE;
  $handler->display->display_options['filters']['name']['expose']['operator_id'] = 'name_op';
  $handler->display->display_options['filters']['name']['expose']['label'] = 'Name';
  $handler->display->display_options['filters']['name']['expose']['operator'] = 'name_op';
  $handler->display->display_options['filters']['name']['expose']['identifier'] = 'name';
  $handler->display->display_options['filters']['name']['expose']['remember_roles'] = array(
    2 => '2',
  );
  /* Filter criterion: Drunkins process: Started */
  $handler->display->display_options['filters']['started']['id'] = 'started';
  $handler->display->display_options['filters']['started']['table'] = 'drunkins_process_run';
  $handler->display->display_options['filters']['started']['field'] = 'started';
  $handler->display->display_options['filters']['started']['operator'] = '>';
  $handler->display->display_options['filters']['started']['exposed'] = TRUE;
  $handler->display->display_options['filters']['started']['expose']['operator_id'] = 'started_op';
  $handler->display->display_options['filters']['started']['expose']['label'] = 'Started at/after';
  $handler->display->display_options['filters']['started']['expose']['operator'] = 'started_op';
  $handler->display->display_options['filters']['started']['expose']['identifier'] = 'started';
  $handler->display->display_options['filters']['started']['expose']['remember_roles'] = array(
    2 => '2',
  );
  /* Filter criterion: Drunkins process: Started */
  $handler->display->display_options['filters']['started_1']['id'] = 'started_1';
  $handler->display->display_options['filters']['started_1']['table'] = 'drunkins_process_run';
  $handler->display->display_options['filters']['started_1']['field'] = 'started';
  $handler->display->display_options['filters']['started_1']['operator'] = '<';
  $handler->display->display_options['filters']['started_1']['exposed'] = TRUE;
  $handler->display->display_options['filters']['started_1']['expose']['operator_id'] = 'started_1_op';
  $handler->display->display_options['filters']['started_1']['expose']['label'] = 'Started before';
  $handler->display->display_options['filters']['started_1']['expose']['operator'] = 'started_1_op';
  $handler->display->display_options['filters']['started_1']['expose']['identifier'] = 'started_1';
  $handler->display->display_options['filters']['started_1']['expose']['remember_roles'] = array(
    2 => '2',
  );

  /* Display: Statsitics */
  $handler = $view->new_display('page', 'Statsitics', 'stats');
  $handler->display->display_options['path'] = 'admin/config/workflow/drunkins/_/process-stats';
  $handler->display->display_options['menu']['type'] = 'tab';
  $handler->display->display_options['menu']['title'] = 'Statistics';
  $handler->display->display_options['menu']['weight'] = '1';
  $handler->display->display_options['menu']['context'] = 0;
  $handler->display->display_options['menu']['context_only_inline'] = 0;

  /* Display: Data export */
  $handler = $view->new_display('views_data_export', 'Data export', 'stats_csv');
  $handler->display->display_options['pager']['type'] = 'none';
  $handler->display->display_options['pager']['options']['offset'] = '0';
  $handler->display->display_options['style_plugin'] = 'views_data_export_csv';
  $handler->display->display_options['path'] = 'admin/config/workflow/drunkins/_/process-stats-csv';
  $handler->display->display_options['displays'] = array(
    'stats' => 'stats',
    'default' => 0,
    'checkpoints' => 0,
  );
  $handler->display->display_options['use_batch'] = 'batch';
  $handler->display->display_options['segment_size'] = '100';

  /* Display: Checkpoints */
  $handler = $view->new_display('page', 'Checkpoints', 'checkpoints');
  $handler->display->display_options['defaults']['title'] = FALSE;
  $handler->display->display_options['title'] = 'Drunkins process checkpoint statistics';
  $handler->display->display_options['defaults']['header'] = FALSE;
  /* Header: Global: Text area */
  $handler->display->display_options['header']['area']['id'] = 'area';
  $handler->display->display_options['header']['area']['table'] = 'views';
  $handler->display->display_options['header']['area']['field'] = 'area';
  $handler->display->display_options['header']['area']['label'] = 'Help';
  $handler->display->display_options['header']['area']['content'] = 'This displays the counter values at each point in time that a process was picked up / paused (by cron).

  We first start (or \'continue starting by fetching more items\') all jobs that need to be started, then afterwards process the queue for each job. This is why the same job can show up twice in the same cron run.

  The "Finished" time is only recorded when switching to a different job or at the end of the cron process. This means that <em>for both phases</em>, the last job\'s recorded "Finished" time could be later than the actual time; it covers processing time for this job <em>plus</em> any other non-Drunkins queues/work.
  ';
  $handler->display->display_options['header']['area']['format'] = 'basic_html';
  $handler->display->display_options['defaults']['fields'] = FALSE;
  /* Field: Drunkins process: ID */
  $handler->display->display_options['fields']['id']['id'] = 'id';
  $handler->display->display_options['fields']['id']['table'] = 'drunkins_process_run';
  $handler->display->display_options['fields']['id']['field'] = 'id';
  $handler->display->display_options['fields']['id']['separator'] = '';
  /* Field: Drunkins process: Parent ID */
  $handler->display->display_options['fields']['pid']['id'] = 'pid';
  $handler->display->display_options['fields']['pid']['table'] = 'drunkins_process_run';
  $handler->display->display_options['fields']['pid']['field'] = 'pid';
  $handler->display->display_options['fields']['pid']['label'] = 'Parent';
  $handler->display->display_options['fields']['pid']['separator'] = '';
  /* Field: Drunkins process: (Queue or job) name */
  $handler->display->display_options['fields']['name_1']['id'] = 'name_1';
  $handler->display->display_options['fields']['name_1']['table'] = 'drunkins_process_run';
  $handler->display->display_options['fields']['name_1']['field'] = 'name';
  $handler->display->display_options['fields']['name_1']['label'] = 'Job name';
  /* Field: Drunkins process: Started */
  $handler->display->display_options['fields']['started']['id'] = 'started';
  $handler->display->display_options['fields']['started']['table'] = 'drunkins_process_run';
  $handler->display->display_options['fields']['started']['field'] = 'started';
  /* Field: Drunkins process: Finished */
  $handler->display->display_options['fields']['finished']['id'] = 'finished';
  $handler->display->display_options['fields']['finished']['table'] = 'drunkins_process_run';
  $handler->display->display_options['fields']['finished']['field'] = 'finished';
  /* Field: Drunkins process counter: Counter name */
  $handler->display->display_options['fields']['name']['id'] = 'name';
  $handler->display->display_options['fields']['name']['table'] = 'drunkins_process_counter';
  $handler->display->display_options['fields']['name']['field'] = 'name';
  /* Field: Drunkins process counter: Count */
  $handler->display->display_options['fields']['count']['id'] = 'count';
  $handler->display->display_options['fields']['count']['table'] = 'drunkins_process_counter';
  $handler->display->display_options['fields']['count']['field'] = 'count';
  $handler->display->display_options['defaults']['filter_groups'] = FALSE;
  $handler->display->display_options['defaults']['filters'] = FALSE;
  /* Filter criterion: Drunkins process: Parent ID */
  $handler->display->display_options['filters']['pid']['id'] = 'pid';
  $handler->display->display_options['filters']['pid']['table'] = 'drunkins_process_run';
  $handler->display->display_options['filters']['pid']['field'] = 'pid';
  $handler->display->display_options['filters']['pid']['group'] = 1;
  $handler->display->display_options['filters']['pid']['exposed'] = TRUE;
  $handler->display->display_options['filters']['pid']['expose']['operator_id'] = 'pid_op';
  $handler->display->display_options['filters']['pid']['expose']['label'] = 'Parent ID';
  $handler->display->display_options['filters']['pid']['expose']['operator'] = 'pid_op';
  $handler->display->display_options['filters']['pid']['expose']['identifier'] = 'pid';
  $handler->display->display_options['filters']['pid']['expose']['remember_roles'] = array(
    2 => '2',
  );
  /* Filter criterion: Drunkins process: (Queue or job) name */
  $handler->display->display_options['filters']['name']['id'] = 'name';
  $handler->display->display_options['filters']['name']['table'] = 'drunkins_process_run';
  $handler->display->display_options['filters']['name']['field'] = 'name';
  $handler->display->display_options['filters']['name']['operator'] = 'starts';
  $handler->display->display_options['filters']['name']['group'] = 1;
  $handler->display->display_options['filters']['name']['exposed'] = TRUE;
  $handler->display->display_options['filters']['name']['expose']['operator_id'] = 'name_op';
  $handler->display->display_options['filters']['name']['expose']['label'] = 'Name';
  $handler->display->display_options['filters']['name']['expose']['operator'] = 'name_op';
  $handler->display->display_options['filters']['name']['expose']['identifier'] = 'name';
  $handler->display->display_options['filters']['name']['expose']['remember_roles'] = array(
    2 => '2',
  );
  /* Filter criterion: Drunkins process: Started */
  $handler->display->display_options['filters']['started']['id'] = 'started';
  $handler->display->display_options['filters']['started']['table'] = 'drunkins_process_run';
  $handler->display->display_options['filters']['started']['field'] = 'started';
  $handler->display->display_options['filters']['started']['operator'] = '>';
  $handler->display->display_options['filters']['started']['exposed'] = TRUE;
  $handler->display->display_options['filters']['started']['expose']['operator_id'] = 'started_op';
  $handler->display->display_options['filters']['started']['expose']['label'] = 'Started at/after';
  $handler->display->display_options['filters']['started']['expose']['operator'] = 'started_op';
  $handler->display->display_options['filters']['started']['expose']['identifier'] = 'started';
  $handler->display->display_options['filters']['started']['expose']['remember_roles'] = array(
    2 => '2',
  );
  /* Filter criterion: Drunkins process: Started */
  $handler->display->display_options['filters']['started_1']['id'] = 'started_1';
  $handler->display->display_options['filters']['started_1']['table'] = 'drunkins_process_run';
  $handler->display->display_options['filters']['started_1']['field'] = 'started';
  $handler->display->display_options['filters']['started_1']['operator'] = '<';
  $handler->display->display_options['filters']['started_1']['exposed'] = TRUE;
  $handler->display->display_options['filters']['started_1']['expose']['operator_id'] = 'started_1_op';
  $handler->display->display_options['filters']['started_1']['expose']['label'] = 'Started before';
  $handler->display->display_options['filters']['started_1']['expose']['operator'] = 'started_1_op';
  $handler->display->display_options['filters']['started_1']['expose']['identifier'] = 'started_1';
  $handler->display->display_options['filters']['started_1']['expose']['remember_roles'] = array(
    2 => '2',
  );
  $handler->display->display_options['path'] = 'admin/config/workflow/drunkins/_/process-checkpoints';
  $handler->display->display_options['menu']['type'] = 'tab';
  $handler->display->display_options['menu']['title'] = 'Checkpoints';
  $handler->display->display_options['menu']['weight'] = '2';
  $handler->display->display_options['menu']['context'] = 0;
  $handler->display->display_options['menu']['context_only_inline'] = 0;

  /* Display: Data export 2 */
  $handler = $view->new_display('views_data_export', 'Data export 2', 'checkpoints_csv');
  $handler->display->display_options['pager']['type'] = 'none';
  $handler->display->display_options['pager']['options']['offset'] = '0';
  $handler->display->display_options['style_plugin'] = 'views_data_export_csv';
  $handler->display->display_options['defaults']['fields'] = FALSE;
  /* Field: Drunkins process: ID */
  $handler->display->display_options['fields']['id']['id'] = 'id';
  $handler->display->display_options['fields']['id']['table'] = 'drunkins_process_run';
  $handler->display->display_options['fields']['id']['field'] = 'id';
  $handler->display->display_options['fields']['id']['separator'] = '';
  /* Field: Drunkins process: Parent ID */
  $handler->display->display_options['fields']['pid']['id'] = 'pid';
  $handler->display->display_options['fields']['pid']['table'] = 'drunkins_process_run';
  $handler->display->display_options['fields']['pid']['field'] = 'pid';
  $handler->display->display_options['fields']['pid']['separator'] = '';
  /* Field: Drunkins process: (Queue or job) name */
  $handler->display->display_options['fields']['name_1']['id'] = 'name_1';
  $handler->display->display_options['fields']['name_1']['table'] = 'drunkins_process_run';
  $handler->display->display_options['fields']['name_1']['field'] = 'name';
  $handler->display->display_options['fields']['name_1']['label'] = 'Job name';
  /* Field: Drunkins process: Started */
  $handler->display->display_options['fields']['started']['id'] = 'started';
  $handler->display->display_options['fields']['started']['table'] = 'drunkins_process_run';
  $handler->display->display_options['fields']['started']['field'] = 'started';
  /* Field: Drunkins process: Finished */
  $handler->display->display_options['fields']['finished']['id'] = 'finished';
  $handler->display->display_options['fields']['finished']['table'] = 'drunkins_process_run';
  $handler->display->display_options['fields']['finished']['field'] = 'finished';
  /* Field: Drunkins process counter: Counter name */
  $handler->display->display_options['fields']['name']['id'] = 'name';
  $handler->display->display_options['fields']['name']['table'] = 'drunkins_process_counter';
  $handler->display->display_options['fields']['name']['field'] = 'name';
  /* Field: Drunkins process counter: Count */
  $handler->display->display_options['fields']['count']['id'] = 'count';
  $handler->display->display_options['fields']['count']['table'] = 'drunkins_process_counter';
  $handler->display->display_options['fields']['count']['field'] = 'count';
  $handler->display->display_options['defaults']['filter_groups'] = FALSE;
  $handler->display->display_options['defaults']['filters'] = FALSE;
  /* Filter criterion: Drunkins process: Parent ID */
  $handler->display->display_options['filters']['pid']['id'] = 'pid';
  $handler->display->display_options['filters']['pid']['table'] = 'drunkins_process_run';
  $handler->display->display_options['filters']['pid']['field'] = 'pid';
  $handler->display->display_options['filters']['pid']['group'] = 1;
  $handler->display->display_options['filters']['pid']['exposed'] = TRUE;
  $handler->display->display_options['filters']['pid']['expose']['operator_id'] = 'pid_op';
  $handler->display->display_options['filters']['pid']['expose']['label'] = 'Parent ID';
  $handler->display->display_options['filters']['pid']['expose']['operator'] = 'pid_op';
  $handler->display->display_options['filters']['pid']['expose']['identifier'] = 'pid';
  $handler->display->display_options['filters']['pid']['expose']['remember_roles'] = array(
    2 => '2',
  );
  /* Filter criterion: Drunkins process: (Queue or job) name */
  $handler->display->display_options['filters']['name']['id'] = 'name';
  $handler->display->display_options['filters']['name']['table'] = 'drunkins_process_run';
  $handler->display->display_options['filters']['name']['field'] = 'name';
  $handler->display->display_options['filters']['name']['operator'] = 'starts';
  $handler->display->display_options['filters']['name']['group'] = 1;
  $handler->display->display_options['filters']['name']['exposed'] = TRUE;
  $handler->display->display_options['filters']['name']['expose']['operator_id'] = 'name_op';
  $handler->display->display_options['filters']['name']['expose']['label'] = 'Name';
  $handler->display->display_options['filters']['name']['expose']['operator'] = 'name_op';
  $handler->display->display_options['filters']['name']['expose']['identifier'] = 'name';
  $handler->display->display_options['filters']['name']['expose']['remember_roles'] = array(
    2 => '2',
  );
  /* Filter criterion: Drunkins process: Started */
  $handler->display->display_options['filters']['started']['id'] = 'started';
  $handler->display->display_options['filters']['started']['table'] = 'drunkins_process_run';
  $handler->display->display_options['filters']['started']['field'] = 'started';
  $handler->display->display_options['filters']['started']['operator'] = '>';
  $handler->display->display_options['filters']['started']['exposed'] = TRUE;
  $handler->display->display_options['filters']['started']['expose']['operator_id'] = 'started_op';
  $handler->display->display_options['filters']['started']['expose']['label'] = 'Started at/after';
  $handler->display->display_options['filters']['started']['expose']['operator'] = 'started_op';
  $handler->display->display_options['filters']['started']['expose']['identifier'] = 'started';
  $handler->display->display_options['filters']['started']['expose']['remember_roles'] = array(
    2 => '2',
  );
  /* Filter criterion: Drunkins process: Started */
  $handler->display->display_options['filters']['started_1']['id'] = 'started_1';
  $handler->display->display_options['filters']['started_1']['table'] = 'drunkins_process_run';
  $handler->display->display_options['filters']['started_1']['field'] = 'started';
  $handler->display->display_options['filters']['started_1']['operator'] = '<';
  $handler->display->display_options['filters']['started_1']['exposed'] = TRUE;
  $handler->display->display_options['filters']['started_1']['expose']['operator_id'] = 'started_1_op';
  $handler->display->display_options['filters']['started_1']['expose']['label'] = 'Started before';
  $handler->display->display_options['filters']['started_1']['expose']['operator'] = 'started_1_op';
  $handler->display->display_options['filters']['started_1']['expose']['identifier'] = 'started_1';
  $handler->display->display_options['filters']['started_1']['expose']['remember_roles'] = array(
    2 => '2',
  );
  $handler->display->display_options['path'] = 'admin/config/workflow/drunkins/_/process-checkpoints-csv';
  $handler->display->display_options['displays'] = array(
    'checkpoints' => 'checkpoints',
    'default' => 0,
    'stats' => 0,
  );
  $handler->display->display_options['use_batch'] = 'batch';
  $handler->display->display_options['segment_size'] = '100';

  $views[$view->name] = $view;

  return $views;
}
