<?php

/**
 * @file
 * Default data tables (Ctools definitions) for drunkins module.
 *
 * Should be moved to regular hook_schema implementations, thereby not
 * depending on the Data module anymore, when I'm done experimenting.
 */

/**
 * Implements hook_data_default(); exposes data table (Ctools) definitions.
 */
function drunkins_data_default() {
  $data_table = new stdClass();
  $data_table->disabled = FALSE; /* Edit this to true to make a default data_table disabled initially */
  $data_table->api_version = 1;
  $data_table->title = 'Drunkins process counter';
  $data_table->name = 'drunkins_process_counter';
  $data_table->table_schema = array(
    'name' => 'drunkins_process_counter',
    'description' => '',
    'fields' => array(
      'process_id' => array(
        'type' => 'int',
        'size' => 'normal',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'description' => '',
      ),
      'name' => array(
        'type' => 'varchar',
        'size' => 'normal',
        'length' => '255',
        'not null' => TRUE,
        'description' => '',
      ),
      'count' => array(
        'type' => 'int',
        'size' => 'normal',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'description' => '',
      ),
    ),
    'primary key' => array(
      0 => 'process_id',
      1 => 'name',
    ),
  );
  $data_table->meta = array(
    'fields' => array(
      'process_id' => array(
        'label' => 'Process ID',
      ),
      'name' => array(
        'label' => 'Counter name',
      ),
      'count' => array(
        'label' => 'Count',
      ),
    ),
    'join' => array(
      'drunkins_process_run' => array(
        'left_field' => 'id',
        'field' => 'process_id',
        'inner_join' => '0',
      ),
    ),
  );

  $tables[$data_table->name] = $data_table;

  $data_table = new stdClass();
  $data_table->disabled = FALSE; /* Edit this to true to make a default data_table disabled initially */
  $data_table->api_version = 1;
  $data_table->title = 'Drunkins process';
  $data_table->name = 'drunkins_process_run';
  $data_table->table_schema = array(
    'name' => 'drunkins_process_run',
    'description' => '',
    'fields' => array(
      'id' => array(
        'type' => 'serial',
        'size' => 'normal',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'description' => '',
      ),
      'pid' => array(
        'type' => 'int',
        'size' => 'normal',
        'unsigned' => TRUE,
        'not null' => FALSE,
        'description' => '',
      ),
      'name' => array(
        'type' => 'varchar',
        'size' => 'normal',
        'length' => '255',
        'not null' => TRUE,
        'description' => '',
      ),
      'started' => array(
        'type' => 'int',
        'size' => 'normal',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'description' => '',
      ),
      'finished' => array(
        'type' => 'int',
        'size' => 'normal',
        'unsigned' => TRUE,
        'not null' => FALSE,
        'description' => '',
      ),
    ),
    'primary key' => array(
      0 => 'id',
    ),
    'indexes' => array(
      'pid' => array(
        0 => 'pid',
      ),
      'name' => array(
        0 => 'name',
      ),
      'started' => array(
        0 => 'started',
      ),
      'finished' => array(
        0 => 'finished',
      ),
    ),
  );
  $data_table->meta = array(
    'fields' => array(
      'id' => array(
        'label' => 'ID',
        'views_field_handler' => 'views_handler_field_numeric',
        'views_filter_handler' => 'views_handler_filter_numeric',
        'views_argument_handler' => 'views_handler_argument_numeric',
        'views_sort_handler' => 'views_handler_sort',
      ),
      'pid' => array(
        'label' => 'Parent ID',
        'views_field_handler' => 'views_handler_field_numeric',
        'views_filter_handler' => 'views_handler_filter_numeric',
        'views_argument_handler' => 'views_handler_argument_numeric',
        'views_sort_handler' => 'views_handler_sort',
      ),
      'name' => array(
        'label' => '(Queue or job) name',
        'views_field_handler' => 'views_handler_field',
        'views_filter_handler' => 'views_handler_filter_string',
        'views_argument_handler' => 'views_handler_argument_string',
        'views_sort_handler' => 'views_handler_sort',
      ),
      'started' => array(
        'label' => 'Started',
        'views_field_handler' => 'views_handler_field_date',
        'views_filter_handler' => 'views_handler_filter_date',
        'views_argument_handler' => 'views_handler_argument_date',
        'views_sort_handler' => 'views_handler_sort_date',
      ),
      'finished' => array(
        'label' => 'Finished',
        'views_field_handler' => 'views_handler_field_date',
        'views_filter_handler' => 'views_handler_filter_date',
        'views_argument_handler' => 'views_handler_argument_date',
        'views_sort_handler' => 'views_handler_sort_date',
      ),
    ),
  );

  $tables[$data_table->name] = $data_table;

  return $tables;
}
