This file does not properly describe the project yet. See
https://www.drupal.org/project/drunkins for that.

For now, just some bits and pieces:

* The reason for the drunkins-release-queue-items command:

It can stop items from being queued forever for jobs running on cron. The
common reason for items staying queued is an exception being thrown while the
item is being processed. This is implemented as a small drush utility (and not
e.g. a setting governing functionality in hook_cron()) because:
- whether items can can safely be reprocessed depends on the specific job;
- the code in this utility interacts with Drupal Core, not with drunkins.

The last point is because if a job runs on cron, it uses the 'queue'
runner/backend. Drunkins inserts items into the queue right after starting the
job and marking it "running"... and then it does nothing else with the queue
anymore. It only registers a cron worker callback. Drupal Core itself (or e.g.
Elysia Cron) picks up the items from the queue. The standard SystemQueue
implementation also sets an 'expire' time which acts as a sort of lock so that
an item is not picked up twice. Actually... SystemQueue never picks up any item
with a non-zero 'expire' time. It does have a method to reset this time, but.
Drupal never calls this method.

This means it is left up to other code to reset the expire time. It seems that
Drupal Core's reasoning (as well as Elysia's) is that it cannot determine
whether an item is allowed to be reprocessed, so it will leave that up to the
'owner' of the specific queue.

This command provides that option. Use it only when you know your specific job
will not cause any side effects by trying to reprocess items that failed once.

We might convert this to a job setting in the future, but not yet. We'll
probably want to gain some more experience first, and/or rewrite some parts of
the 'runner' code and the interaction with the queue mechanism.
