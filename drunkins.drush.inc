<?php

/**
 * @file
 * Provides drush commands to set up a site for Acquia Site Factory.
 */

/**
 * Implements hook_drush_command().
 */
function drunkins_drush_command() {
  return array(
    'drunkins-run-job' => array(
      'description' => dt('Runs a full drunkins job (starting, processing all items and finishing it).'),
      'arguments' => array(
        'job-id' => dt('The ID (machine name) of the job.'),
      ),
      'required-arguments' => TRUE,
      'options' => array(
        // 'drush help drunkins-run-job' apparently cuts documentation off at a
        // maximum length, so we move the runner help into the command. The
        // 'help' value is actually arbitrary because we just get the help text
        // when specifying a nonexistent runner name.
        'runner' => dt("The runner/backend to run the job on. Specify '--runner help' for more info."),
        'settings-json' => dt('Settings to override the default ones for this job, in JSON format.'),
        'runner-settings-json' => dt('Runner settings to override the default ones for this job run, in JSON format. ("progressive" is ignored.)'),
      ),
    ),
    'drunkins-start-job' => array(
      'description' => dt('Starts a drunkins job in queue mode. (Other mechanisms, e.g. cron, will be responsible for running it.)

If the standard job configuration does not make it run on cron, \'{"force_cron":true}\' can be passed in --runner-settings-json.'),
      'arguments' => array(
        'job-id' => dt('The ID (machine name) of the job.'),
      ),
      'required-arguments' => TRUE,
      'options' => array(
        'settings-json' => dt('Settings to override the default ones for this job, in JSON format.'),
        'runner-settings-json' => dt('Runner settings to override the default ones for this job run, in JSON format.'),
      ),
    ),
    'drunkins-reset-job' => array(
      'description' => dt("Resets a running drunkins job and deletes all items that are still queued.

A batch process' items cannot be deleted with this command. The job will be 'unlocked' but if the batch process is currently still being run by another PHP process, this has unknown consequences."),
      'arguments' => array(
        'job-id' => dt('The ID (machine name) of the job.'),
      ),
      'required-arguments' => TRUE,
      'options' => array(
        'minimum-run-time' => dt('Resets the job only if it was started at least this number of seconds ago.'),
      ),
    ),
    'drunkins-release-queue-items' => array(
      'description' => dt("Releases 'claimed but expired' items for a running job from Drupal's SystemQueue, so cron can reprocess them. (See README.)"),
      'arguments' => array(
        'job-id' => dt('The ID (machine name) of the job.'),
      ),
      'required-arguments' => TRUE,
      'options' => array(
        'expire-threshold' => dt("By default, all expired items (with an 'expire' value smaller than the current time) will be released. Providing an positive number N will only release items which already expired at least N seconds ago; providing a negative number -N will also release items that will expire within N seconds."),
        'quiet' => dt('Do not output the number of items affected.'),
      ),
    ),
  );
}

/**
 * Command callback: resets a Drunkins job.
 */
function drush_drunkins_reset_job($job_id, $unrecognized = NULL) {
  if (isset($unrecognized)) {
    return drush_set_error(dt('Extraneous argument received: @arg', array('@arg' => $unrecognized)));
  }

  $info = drunkins_job_status_info($job_id);
  if (!$info) {
    return drush_set_error(dt('No job found named "@job".', array('@job' => $job_id)));
  }
  if ($info['running'] != 3) {
    drush_print(dt('Job "@job" is not running.', array('@job' => $job_id)));
    return TRUE;
  }
  $min_time = drush_get_option('minimum-run-time', 0);
  if ($min_time) {
    // Arbitrary check, must never fail in practice since 'started' is always
    // populated. (The timestamp is 2015-01-01.)
    if ($info['started'] < 1420070400 || $info['started'] > time()) {
      return drush_set_error(dt('Job "@job" has unknown start time "@started".', array('@job' => $job_id, '@started' => $info['started'])));
    }
    // Calculate from the start of this request,
    if (REQUEST_TIME - $info['started'] < $min_time) {
      drush_print(dt('Job "@job" has only been running for @time seconds.', array('@job' => $job_id, '@time' => REQUEST_TIME - $info['started'])));
      return TRUE;
    }
  }

  drunkins_reset_job($job_id);
}

/**
 * Command callback: fully runs a Drunkins job.
 */
function drush_drunkins_run_job($job_id, $unrecognized = NULL) {
  if (isset($unrecognized)) {
    return drush_set_error(dt('Extraneous argument received: @arg', array('@arg' => $unrecognized)));
  }

  $runner = drush_get_option('runner', 'default');
  switch ($runner) {
    case 'help':
      // If this would be in the option documentation, Drush woult cut it off.
      drush_print(dt("Possible values for the --runner option:
* 'default' is the default backend, storing all items temporarily in a Drupal queue, then handling them all in a single PHP process run.
* 'batch' uses Drush' batch backend. This may use less resources though it spawns child processes.
* 'single-batch' process the job as a non-progressive batch using the Core backend. This is mainly for testing; there are no known advantages over the default."));
      return TRUE;

    case 'default':
      // We might remove 'drush' at some point and just call it 'queue' again;
      // it's essentially a non-progressive 'queue' with an extra log message.
      // So having a separate $method is kind-of inconsistent with 'batch'.
      return _drush_drunkins_start_job($job_id, 'drush', array('progressive' => FALSE));

    case 'batch':
      // Code in drush.module/run.inc has switches to see whether it should run
      // the drush backend or the normal batch backend. It looks like we can
      // leave it this way because there's a lot of code reuse and very little
      // divergence; we'll make the call when we convert the code to runner
      // classes.
      return _drush_drunkins_start_job($job_id, 'batch', array('redirect_after_start' => TRUE));

    case 'single-batch':
      // The Drush batch backend has no progressive=FALSE; this calls Core
      // code.
      return _drush_drunkins_start_job($job_id, 'batch', array('progressive' => FALSE));

    default:
      return drush_set_error(dt("Unknown runner/backend type '$runner'."));
  }
}

/**
 * Command callback: starts a Drunkins job.
 */
function drush_drunkins_start_job($job_id, $unrecognized = NULL) {
  if (isset($unrecognized)) {
    return drush_set_error(dt('Extraneous argument received: @arg', array('@arg' => $unrecognized)));
  }
  return _drush_drunkins_start_job($job_id, 'queue');
}

/**
 * Starts or runs a Drunkins job.
 *
 * @param string $job_id
 *   The job ID.
 * @param string $method
 *   Way of running this job: 'queue' to only start, 'drush' for a drush command
 *   that just runs everything in a single process, and 'batch' to run things
 *   through drush' batch backend.
 * @param array $override_runner_settings
 *   (optional) Runner settings that override even the drush option.
 * @return bool
 */
function _drush_drunkins_start_job($job_id, $method, array $override_runner_settings = NULL) {
  $settings = drush_get_option('settings-json', array());
  if ($settings) {
    $settings = json_decode($settings, TRUE);
    if ($settings === NULL) {
      return drush_set_error(dt('The settings-json value does not contain valid JSON.'));
    }
  }

  $runner_settings = drush_get_option('runner-settings-json', array());
  if ($runner_settings) {
    $runner_settings = json_decode($runner_settings, TRUE);
    if ($runner_settings === NULL) {
      return drush_set_error(dt('The settings-json value does not contain valid JSON.'));
    }
  }
  if (isset($override_runner_settings)) {
    $runner_settings = $override_runner_settings + $runner_settings;
  }

  // We always want to see "There are no items to process." so override any
  // log severity for that message.
  // @TODO that's not the case when we make a drush command run on cron, so
  // investigate what we should do here. Maybe just add an example
  // 'log_severity_no_items' for --runner-settings-json?
  if (!isset($runner_settings['log_severity_no_items'])) {
    $runner_settings['log_severity_no_items'] = isset($runner_settings['log_level']) ? $runner_settings['log_level'] : WATCHDOG_INFO;
  }

  // Change logging settings. 'screen' means drupal_set_message so remove that.
  if (isset($runner_settings['logging'])) {
    if ($runner_settings['logging'] & DrunkinsJob::LOG_SCREEN) {
      $runner_settings['logging'] -= DrunkinsJob::LOG_SCREEN;
    }
    $runner_settings['logging'] |= DrunkinsJob::LOG_DRUSH;
  }
  else {
    $runner_settings['logging'] = DrunkinsJob::LOG_DRUSH | DrunkinsJob::LOG_WATCHDOG;
  }

  // @TODO explicitly drush_set_option('verbose'? Because otherwise, we won't
  //   see logs with NOTICE/INFO, which is dangerous in this command.
  // @TODO if drush -d flag set, then also change severity threshold for drunkins? <-- note that jobs
  //   themselves have no severity threshold - just drunkins itself. But that's fine here... mostly.
  // @TODO conversely, if -q flag set, then suppress printing watchdog logs _for drunkins job starts_ too?
  //   (See comments in __drunkins_log().)
  //
  // @TODO should we even keep the DRUSH_LOG option, now that we finally
  //   understand the interaction between watchdog(), drush_log() and drush options?
  //   (Note I believe the 3 above TODOs are independent of deleting DRUSH_LOG
  //   because they still go for the LOG_WATCHDOG stuff. If I'd known this and
  //   knew what watchdog()->drush_log() was doing I likely would have never
  //   introduced LOG_DRUSH.)

  $ret = drunkins_start_job($job_id, $method, $runner_settings, $settings);

  if ($ret && $method === 'queue') {
    // For the remainder of the process, logging should be different.
    // The process state, including runner settings, have not been saved
    // into the database yet, so assume that will get done automatically.
    $runner_settings['logging'] -= DrunkinsJob::LOG_DRUSH;
    _drunkins_set_process_config($job_id, 'runner', $runner_settings);
  }

  return $ret;
}

/**
 * Command callback: fully runs a Drunkins job.
 */
function drush_drunkins_release_queue_items($job_id, $unrecognized = NULL) {
  if (isset($unrecognized)) {
    return drush_set_error(dt('Extraneous argument received: @arg', array('@arg' => $unrecognized)));
  }

  $quiet = drush_get_option('quiet', FALSE);
  $threshold = drush_get_option('expire-threshold', 0);
  if ($threshold) {
    if (!is_numeric($threshold)) {
      return drush_set_error(dt('expire-threshold option must be numeric.'));
    }
  }

  // Something's off with this interface. There is no defined standard way of
  // seeing which items in a queue have the 'expire' property set. There is a
  // defined method for resetting the 'expire' property (releaseItem()), but
  // that has the item as input. And we can never get the item in the first
  // place (because we can only get items with expire == 0). Just do a single
  // update query...
  $affected = db_update('queue')
    ->fields(array('expire' => 0))
    ->where('name = :name AND expire > 0 AND expire <= :exp', array(':name' => $job_id, ':exp' => time() - (int) $threshold))
    ->execute();
  if (!$quiet) {
    drush_print("$affected items released.");
  }
}
