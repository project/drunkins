<?php

/**
 * Exception representing a temporary error.
 *
 * This can be thrown by jobs for suggesting that an action (fetching or
 * processing data) should be retried later, rather than cancelled.
 *
 * A suggested 'backoff period' (in seconds) can be provided. It's up to the
 * caller / the specific job runner to use or ignore this value. If thrown
 * during processing, it's also up to the job runner and/or job settings,
 * whether the item gets retried immediately, discarded, or queued while other
 * items are tried first.
 *
 * It is the job's responsibility to make sure the context is in a good state
 * for the action to be retried (or for the next item to be processed). This
 * could need some attention when throwing this exception during the 'start'
 * phase.
 *
 * @todo implement this better across all jobs / the runner. We've started slow
 *   and only implemented this for retrying repeated start() calls, in our
 *   default runner. Proper handling of processItem() (or maybe even finish()?)
 *   might wait until we rewrite the runner code.
 */
class DrunkinsTemporaryError extends RuntimeException {

  /**
   * Default backoff time (suggested number of seconds to pause processing).
   *
   * Note this represents the time to pause any processing, not the time to
   * keep this item queued until retrying.
   * This is a fairly random number; we can't make it ideal for all jobs but
   * it's only a suggested number anyway.
   */
  const DEFAULT_SUGGESTED_WAIT_SECONDS = 5;

  /**
   * Suggested number of seconds for the caller to wait until retry.
   *
   * @var int
   */
  protected $suggestedWait;

  /**
   * DrunkinsTemporaryError constructor.
   *
   * @param string $message
   *   Exception message.
   * @param int $suggest_wait_seconds
   *   (Optional) number of seconds for the caller to (ideally) wait until
   *   retrying.
   * @param int $code
   *   (Optional) exception code.
   * @param \Throwable $previous
   *   (Optional) previous exception.
   */
  public function __construct($message = "", $suggest_wait_seconds = self::DEFAULT_SUGGESTED_WAIT_SECONDS, $code = 0, $previous = NULL) {
    $this->suggestedWait = filter_var($suggest_wait_seconds, FILTER_VALIDATE_INT) === FALSE
      ? self::DEFAULT_SUGGESTED_WAIT_SECONDS : $suggest_wait_seconds;
    parent::__construct($message, $code, $previous);
  }

  /**
   * Returns the suggested number of seconds for the caller to wait until retry.
   *
   * @return int
   */
  public function getSuggestedWait() {
    return $this->suggestedWait;
  }

}
