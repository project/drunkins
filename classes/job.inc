<?php
/**
 * @file
 * Contains DrunkinsJobInterface and DrunkinsJob.
 */

/**
 * Defines an interface for jobs that are run through a Drunkins 'runner'.
 *
 * A process (meaning: the consecutive calls of a job's start(), processItem()
 * and finish() methods with(in) the same context) could instantiate a job class
 * many times. Jobs should assume that they may, or may not, be reinstantiated
 * before every call to start(), processItem() or finish().
 *
 * A configuration/settings array is passed to the constructor; its values are
 * the same throughout the process.
 *
 * This implies the following:
 *
 * - State/context information for a process, which is shared between
 *   consecutive calls, must not be stored in property variables. A class must
 *   not assume that these variables keep their values on consecutive calls to
 *   any class method.
 *   Set/change state in the $context argument of a method; this will be kept.
 *
 * - A class must also not assume that property variables are reset to their
 *   initial values on every call to any class method.
 *
 * - Constructor functions should not do heavy processing. If preprocessing the
 *   settings is expensive, consider doing this in start() and storing the
 *   processed result in $context.
 *
 * In other words: a job class should be able to be implemented without any
 * properties, except for the configuration/settings array which is passed to
 * the constructor function. It may still be a good idea to have properties
 * which have some sort of 'cache' function; this could save time because in
 * practice the same class instance will be reused over a number of calls to
 * processItem(). One should just not depend on this cache being 'warm' at any
 * time.
 *
 * This interface currently extends DrunkinsFetcherInterface. If this formal
 * relationship ever changes, this won't affect the methods defined by
 * DrunkinsJobInterface. They will stay the same; start() / finish() may just be
 * moved back into DrunkinsJobInterface, and/or the 'extends' relationship
 * may be broken, if needed.
 */
interface DrunkinsJobInterface extends DrunkinsFetcherInterface {

  /**
   * Processes a single item (which was probably retrieved by start()).
   *
   * No value is expected to be returned, unless otherwise agreed between caller
   * and class (by some mechanism outside this specification). Any error
   * encountered should be raised by throwing an exception. No custom exception
   * classes have been defined (yet?) for this and code using fetcher/job
   * classes should be able to handle any kind of exception.
   *
   * @param $item
   *   The (one) item to process now.
   * @param array $context
   *   Context for the process.
   *
   * @throws \Exception
   *   For any error that is supposed to prevent the process from being regarded
   *   as 'processed'.
   */
  public function processItem($item, array &$context);

/* TODO [...this is a very old note that does not make sense anymore but it's
 *       left here until I can deal with it in its entirety ] make text on what
 * to do when PHP's execution time is reached during cron-queue processing of an
 * item? But it's not much use currently; because of the lease time of 3600
 * seconds, the caller will discard this one item still in the queue anyway -
 * except if it happens to be the one marked 'last'.
 *
 * Actually, we should write down a blurb about losing items. This process is
 * 'lossy'; items are discarded after a processItem() call (except when an
 * exception is thrown). Where it matters, we've caught that in practice
 * so far because we already needed a 'backup clean-up' job anyway, to go
 * through existing entities and reprocess the ones that didn't finish. In
 * other words, because we never had really-unique-atomic data in items in a
 * queue. But what if we do? I guess we should implement QueueJobSerialQueue
 * or something like it.

// TODO make our own Queue implementation (QueueJobSerialQueue implements DrupalReliableQueueInterface?)
// which should prevent 2 threads from taking the same context information == process items really synchronously,
// to protect $context from becoming garbled. (ORDER of items also guaranteed? In principle not, but we can build that in?)
// When we're doing this anyway - make our own queue table? with name == job_id?
 */

}

/**
 * Abstract (example/base) class implementing some helper methods/properties
 * useful for most Drunkins Jobs.
 */
abstract class DrunkinsJob implements DrunkinsJobInterface {
  use DrunkinsProcessSummaryTrait;

  /**
   * Report errors to watchdog.
   */
  const LOG_WATCHDOG = 1;

  /**
   * Report errors to the screen using drupal_set_message().
   */
  const LOG_SCREEN = 2;

  /**
   * Report errors using drush_log().
   *
   * (This can probably cause duplicate watchdog logs in some drush contexts?)
   */
  const LOG_DRUSH = 4;

  /**
   * Settings that may influence a specific process (i.e. a 'run' of a job).
   *
   * In common usage, these may come from: (last overrides first)
   * - settings configured in hook_queue_info();
   * - settings configured in a configForm; (TODO configForm does not exist yet. TODO look at naming of configForm & settingsForm? compare Feeds.
   * - runtime settings submitted through settingsForm for batch process
   *   (not for cron process because there is no way for already-running
   *   cron processes to get these settings. Yet?);
   * - any custom code calling drunkins_get_job().
   *
   * There is no strict conceptual distinction between 'variables' stored
   * in $this->settings, and those stored in custom properties. It's up to the
   * implementing class to decide what works best.
   * From a practical standpoint, everything in $settings can be left for
   * general code to set, and custom properties must somehow be set by the class
   * (usually the constructor method) itself.
   *
   * Settings used by this class:
   * - logging (int):
   *   Whether log() directs errors to screen and/or watchdog. See constants.
   *
   * - job_id (string):
   *   Identifier string that the caller can give to this specific job.
   *   Optional, but some extended functionality needs it to retrieve settings
   *   specific to a job. (The caller may have defined different jobs using the
   *   same class, so classname does not suffice.) Example: cacheGet() below.
   *   job_id is always set if you instantiate a job using drunkins_get_job().
   *
   * - drunkins_fetcher_class (string):
   *   classname of a DrunkinsFetcherInterface implementation which will take
   *   care of fetching items.
   *
   * - opt_list: (mixed)
   *   Enables viewing the fetched items on the screen or exporting them to
   *   Excel (instead of processing them).
   *   TRUE: enable listing in raw format. $settings['list_format'] = 'raw' will
   *     be set when start() is called only to list items.
   *   array: different possible formats for listing; the key will be set in
   *     $settings['list_format']; the value is a short description for the UI.
   *   If you set this, you are responsible for outputting content that's
   *   safe for display when $settings['list_format'] is set!
   * @todo remove this comment and create check_plain()ed output by default,
   * overridable in custom UI or so
   *
   * @var array
   */
  protected $settings;

  /**
   * Job settings which won't be passed to the 'display' function as URL
   * arguments; see settingsFormSubmit(). The display page (as implemented by
   * drunkins_list_items()) gets $this->settings passed in the URL; if you see
   * any that you don't want passed, add them to this property.
   *
   * @var array
   */
  protected $display_arguments_filter;

  /**
   * Constructor.
   *
   * Jobs should be treated as if they could be re-instantiated very often, so
   * heavy processing is discouraged here. For more restrictions, see the
   * comments at the interface definition.
   *
   * @param array $settings
   *   The initial process settings. See property definition (or comments at the
   *   top of extending classes, hopefully) for supported keys.
   */
  public function __construct(array $settings = array()) {

    if (!isset($settings['logging'])) {
      $settings['logging'] = self::LOG_WATCHDOG;
    }
    $this->settings = $settings;

    // In case a child class sets 'opt_list', we filter away arguments which
    // are useless in the 'display' URL.
    $this->display_arguments_filter = array(
      'job_id',
      'drunkins_fetcher_class',
      'cron',
      'cron_force',
      'display',
      'opt_list'
    );
  }

  /**
   * settingsForm(); not part of interface definition; may move to own UI class.
   *
   * It's up to the caller to collect these settings and pass them as the
   * constructor's $settings argument, after which other code can access them.
   * (drunkins module takes care of this.)
   */
  public function settingsForm() {

    // If fetcher class is defined, merge form definition from it.
    $form = $this->callFetcherMethod('settingsForm', array());

    if (!empty($this->settings['opt_list'])) {
      $form['display'] = array(
        '#type'        => 'fieldset',
        '#collapsible' => TRUE,
        '#title'       => t('Display items (without processing them)'),
        '#description' => t('The dataset displayed could be dependent on options above.'),
        '#weight'      => 101,
      );
      $form['display']['output_format'] = array(
        '#title' => t('Output to'),
        '#type'  => 'select',
        '#options' => array(
          'screen' => t('Screen'),
          'xls' => t('Excel'),
        ),
        '#weight'      => 1,
      );
      if (is_array($this->settings['opt_list'])) {
        // The class has provided different options
        $form['display']['list_format'] = array(
          '#title'   => t('Data format'),
          '#type'    => 'select',
          '#options' => $this->settings['opt_list'],
          '#weight'  => 3,
        );
      }
      else {
        $form['display']['list_format'] = array(
          '#type'  => 'value',
          '#value' => 'raw',
        );
      }
      $form['display']['display_rerun'] = array(
        '#type' => 'checkbox',
        '#title' => t('Rerun fetch command if needed, to guarantee a full dataset. (This could cause timeouts / memory issues.)'),
        // This is not a job setting but we'll temporarily support it (until
        // this goes away), if the job config wants to provide a default when
        // it's sure timeouts won't happen.
        '#default_value' => !empty($this->settings['fetcher_display_rerun']),
        '#weight' => 4,
      );
      $form['display']['display'] = array(
        '#type'   => 'submit',
        '#value'  => t('Display'),
        '#weight' => 9,
      );
    }

    return $form;
  }

  /**
   * Implements DrunkinsJobInterface::start().
   *
   * An extending class must either implement its own code in start() and not
   * call this parent method, or make sure that a fetcher class is always set.
   */
  public function start(array &$context) {

    // This base job has code for supporting 'can_override_fetch' capability, if
    // a child class does not implement its own start(). The capability must
    // still be explicitly advertised by the child class / configuration.
    if (!empty($context['drunkins_override_fetch'])) {
      if (!is_array($context['drunkins_override_fetch'])) {
        throw new \Exception("'drunkins_override_fetch' context-value is not an array!");
      }
      return $context['drunkins_override_fetch'];
    }

    // If fetcher class is defined, use it for fetching items.
    $items = $this->callFetcherMethod('start', NULL, $context);
    if (!isset($items)) {
      throw new RuntimeException('No fetcher class defined or start() method implemented.');
    }

    return $items;
  }

  /**
   * Finalizes processing after a batch or queue process has fully finished.
   *
   * @param array $context
   *   Context for the process.
   *
   * @return string
   *   Result message, which the caller can print / log.
   *
   * @throws \RuntimeException
   *   For any error that prevents the process from finishing cleanly. (It is up
   *   to the caller to handle this as desired.)
   *
   * @todo change the return value to void-by-default. Log messages instead.
   */
  public function finish(array &$context) {
    // If fetcher class is defined, try to call finish().
    return $this->callFetcherMethod('finish', '', $context);
  }

  /// Extra public method, though not defined in an interface:

  /**
   * 'submit function' for any submit buttons you would have added to
   * settingsForm. Does not need to be implemented unless you have added your
   * own buttons.
   * The form values can be found in $this->settings.
   *
   * An implementation of this method can:
   * - use drupal_goto to redirect to other pages
   * - return a string containing (safe/sanitized!) HTML output that will be
   *   added to the page above the settingsForm, which is being rebuilt by the
   *   caller. Note form will not be redirected (from the current POST request
   *   to a GET request) and re-initialized, as is usually the case!
   *
   * @param $op
   *   The value of the pressed button (i.e. $form_state['input']['op'])
   *
   * @return null|string
   *   HTML to render above the settingsForm
   */
  public function settingsFormSubmit($op) {
    if ($op == t('Display')) {

      // Redirect, and pass our 'known' settings in query strings.
      $settings = array_diff_key($this->settings,
        array_flip($this->display_arguments_filter));
      // This is a hackish way of filtering out any 'fetcher settings' which
      // were set in a hook_queue_info implementation: the fetcher class knows
      // about them.
      if (isset($this->settings['drunkins_fetcher_class'])
              && is_string($this->settings['drunkins_fetcher_class'])
              && class_exists($this->settings['drunkins_fetcher_class'])) {
        $fetcher = new $this->settings['drunkins_fetcher_class']($this->settings, $this);
        if (isset($fetcher->display_arguments_filter)) {
          $settings = array_diff_key($settings,
                  array_flip($fetcher->display_arguments_filter));
        }
      }

      if ($settings) {
        drupal_goto('admin/config/workflow/drunkins/' . $this->settings['job_id'] . '/display',
          array('query' => $settings));
      }
      else {
        drupal_goto('admin/config/workflow/drunkins/' . $this->settings['job_id'] . '/display');
      }
    }
    else {
      $this->log('Unknown operation %op!', array('%op' => $op), WATCHDOG_ERROR);
    }
  }

  /// Extra helper methods, for extending classes to call:

  /**
   * Call method in the fetcher class, only if a separate fetcher class is set,
   * and the corresponding method exists.
   *
   * @param string $method
   *   Method name
   * @param mixed $default_value
   *   Value to return from this function if the method does not exist.
   * @param array $context
   *   The process context; must be supplied when the method has it. (At least
   *   'start' and 'finish' do, plus some other 'unofficial' methods.)
   *
   * @return mixed
   *   Return value from the method, or $default_value.
   */
  protected function callFetcherMethod($method, $default_value, array &$context = array()) {
    if (isset($this->settings['drunkins_fetcher_class'])
        && is_string($this->settings['drunkins_fetcher_class'])
        && class_exists($this->settings['drunkins_fetcher_class'])) {

      $fetcher = new $this->settings['drunkins_fetcher_class']($this->settings, $this);
      // is_callable() returns FALSE at least for 'settingsForm'. Beats me.
      if (method_exists($fetcher, $method)) {
        // PHP won't complain when $method does not actually have arguments, so
        // yay. The below should be changed (or $context renamed) if there's
        // ever a function with another first argument.)
        return $fetcher->$method($context);
      }
    }

    return $default_value;
  }

  /**
   * Logs or prints a message.
   *
   * @param string $message
   *   Untranslated message, optionally containing placeholders for translation.
   * @param array $variables
   *   (optional) Replacements to make after translation.
   * @param int $severity
   *   (optional) Severity constant; see watchdog_severity_levels().
   * @param bool $repeat
   *   (optional) If this is FALSE and the message is already set, then the
   *   message won't be repeated.
   *
   * @see t()
   * @see watchdog_severity_levels()
   *
   * @todo this method does not have access to a 'log threshold' setting
   *   (unlike _drunkins_log() which has the runner setting 'log_level', which
   *   should be renamed to 'log_threshold' but which might also disappear when
   *   we switch to PSR-3 logging? Haven't decided yet whether this wrapper
   *   method should reference a per-job threshold setting.)
   */
  public function log($message, array $variables = array(), $severity = WATCHDOG_NOTICE, $repeat = TRUE) {
    // Until we use a PSR-3 logger: reuse code by calling a global function.
    $job_id = isset($this->settings['job_id']) ? $this->settings['job_id'] : '';
    if (!isset($repeat)) {
      // Bad code which will confuse __drunkins_log(). NULL would behave the
      // same as FALSE so use that.
      $repeat = FALSE;
    }
    __drunkins_log($job_id, $message, $variables, $severity, $this->settings, $repeat);
  }

}
