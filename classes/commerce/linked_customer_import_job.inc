<?php
/**
 * @file
 * Contains DrunkinsLinkedCommerceCustomerProfileImportJob.
 */

/**
 * Class DrunkinsLinkedCommerceCustomerProfileImportJob
 *
 * This job handles non-updatable customer profiles in a different way than the
 * standard importer.
 *
 * Non-updatable here means 'attached to an order which has non-cart status'.
 *
 * It can be used to import customer profiles as standalone entities, but also
 * as entities which are linked to a 'main' entity. (A  usual configuration is
 * having the main entity type be 'user' and having two linked entity types,
 * both 'commerce_customer_profile', with 'shipping' and 'billing' as bundles.)
 *
 * The default DrunkinsEntityImportJob selects all linked profiles but
 * silently skips saving non-updatable profiles, so at least there's no risk of
 * creating endless duplicate profiles. (See comments at its saveEntity().)
 *
 * However, you likely want something different, like these scenarios:
 *
 * - select only the profiles with status == 1 (which means if a user has only
 *   deactivated profiles, an import will create a new profile). When updating
 *   a profile which is non-updatable, set status=0 and save a new one.
 * This would work well if you tend to have only one active profile (per profile
 * type), and don't want to end up with multiple active ones. Setting status=0
 * for the existing non-updatable profile could be a bit brittle, however,
 * depending on your configuration. See e.g. https://www.drupal.org/node/2169863
 *
 * - select only the updatable profiles (which means if a user has only
 *   non-updatable profiles, an import will create a new profile). Do not do any
 *   status updates.
 * This means any new profiles with address changes will appear next to any
 * still-enabled older profiles, in e.g. the selection for Commerce Addressbook.
 * The older profiles can still be used (unchanged; changing them manually will
 * create an extra profile). Deactivating an old profile is left up to the user.
 *
 * - we can also add 'status == 1' to this last case, which changes the behavior
 *   in the unlikely case that all updatable profiles for a user are disabled.
 *   Without the 'status == 1' filter, this will update the disabled profile;
 *   with the filter, it will create a new enabled one.
 *
 * This class follows the second scenario by default. It has settings to turn
 * this behavior off and turn on the 'disabling' from the first scenario, but
 * you'll still need to add import_entity_query_filter_field:N and
 * import_entity_query_filter_value:N settings, to resolve only profiles with
 * status=1.
 */
class DrunkinsLinkedCommerceCustomerProfileImportJob extends DrunkinsLinkedEntityImportJob {

  /* Settings in $this->settings, used by this class:
   *
   * - import_profile_disable_non_updatable (boolean):
   *   If TRUE, checks for 'updatability' of a changed profile. If not updatable
   *   (according to commerce_order_commerce_customer_profile_can_delete()),
   *   then disable this profile before saving a new one. See comments at the
   *   top of this class and DrunkinsEntityImportJob::saveEntity().
   *
   * - import_profile_resolve_only_updatable (boolean, default = TRUE):
   *   Rereturn only 'updatable' profile IDs from resolveEntityIds().
   */

  public function __construct(array $settings = array()) {

    if (!isset($settings['import_profile_resolve_only_updatable'])) {
      $settings['import_profile_resolve_only_updatable'] = TRUE;
    }

    parent::__construct($settings);

    // In case a subclass sets 'opt_list', we filter away arguments which
    // are useless in the 'display' URL.
    $this->display_arguments_filter = array_merge($this->display_arguments_filter, array(
      'import_profile_disable_non_updatable',
      'import_profile_resolve_only_updatable',
    ));
  }

  /**
   * {@inheritdoc}
   */
  protected function saveEntity($entity, array $changed_parts = array('*' => TRUE)) {

    // No custom behavior if the entity has not changed.
    $old_id = $this->getEntityId($entity);
    if (!$changed_parts) {
      return $old_id;
    }

    if ($this->settings['import_entity_type'] === 'commerce_customer_profile'
        && !empty($this->settings['import_profile_disable_non_updatable'])
        && $old_id
        && function_exists('commerce_order_commerce_customer_profile_can_delete')
        && !commerce_order_commerce_customer_profile_can_delete($entity)) {

      // Override parent behavior: load the old entity and disable it.
      $old_entity = entity_load('commerce_customer_profile', array($old_id), array(), TRUE);
      if (!empty($old_entity->status)) {
        $old_entity->status=0;
        commerce_customer_profile_save($old_entity);
      }
      // NOTE: If you now have an new entity (with new ID) with status=0 and
      // the old entity still has status=1... you'll need to patch commerce
      // and/or addressfield module.

      // Save new entity.
      $entity->profile_id = NULL;
    }

    return parent::saveEntity($entity, $changed_parts);
  }

  /**
   * {@inheritdoc}
   */
  protected function executeResolveEntityIdsQuery(array $src_ids, $entity_type, array $options) {

    $data = parent::executeResolveEntityIdsQuery($src_ids, $entity_type, $options);

    $return_type = isset($options['query_type']) ? $options['query_type']
      : (isset($options['return_type']) ? $options['return_type'] : '');

    if (!empty($this->settings['import_profile_resolve_only_updatable'])
        && function_exists('commerce_order_commerce_customer_profile_can_delete')) {

      if (!$entity_type) {
        $entity_type = $this->settings['import_entity_type'];
      }

      // If the main entity is profile, remove non-updatable profiles from the
      // result set / cache.
      if ($entity_type === 'commerce_customer_profile') {

        $ids = array();
        if (empty($options['requery_all'])) {
          if ($this->settings['import_ids_case_sensitive']) {
            $src_ids = array_map('strtoupper', $src_ids);
          }

          // Preload all resolved entities in one go, for speedup.
          if (isset($this->entityIds)) {
            foreach ($src_ids as $src_id) {
              $ids = array_merge($ids, $this->entityIds[$options['cache_bin']][$src_id]);
            }
          }
          elseif ($return_type === 'linked') {
            $ids = array_keys($data);
          }
          elseif ($return_type === 'keyed') {
            foreach ($data as $src_id => $eids) {
              $ids = array_merge($ids, $eids);
            }
          }
          else {
            $ids = $data;
          }
          if ($ids) {
            entity_load('commerce_customer_profile', $ids);
          }
        }

        // Now delete non-updatable profiles from the result.
        if ($ids || !empty($options['requery_all'])) {

          if (isset($this->entityIds) || $return_type === 'keyed') {
            // $data is the same format as the cache.
            if (isset($this->entityIds)) {
              $data =& $this->entityIds[$options['cache_bin']];
            }

            // Remove the unwanted values from the cache and/or return arg.
            // If caching is on, only go through the items just added.
            if (!empty($options['requery_all']) || $return_type === 'keyed') {
              $src_ids = array_keys($data);
            }

            foreach ($src_ids as $src_id) {
              if (!empty($data[$src_id])) {
                // Recreate IDs, don't just unset values because we want to keep
                // 'normal' 0-based keys just to be sure.
                $ids = array();
                foreach (entity_load('commerce_customer_profile', $data[$src_id]) as $profile) {
                  if (commerce_order_commerce_customer_profile_can_delete($profile)) {
                    $ids[] = $profile->profile_id;
                  }
                }
                $data[$src_id] = $ids;
              }
            }
          }
          elseif ($return_type == 'linked') {
            // Remove the unwanted IDs, which are array keys, from $data.
            foreach (array_keys($data) as $main_eid) {
              $profile = commerce_customer_profile_load($main_eid);
              if (!$profile || !commerce_order_commerce_customer_profile_can_delete($profile)) {
                unset($data[$main_eid]);
              }
            }
          }
          else {
            // Remove the unwanted values from $data. $ids is equal to $data.
            $data = array();
            foreach (entity_load('commerce_customer_profile', $ids) as $profile) {
              if (commerce_order_commerce_customer_profile_can_delete($profile)) {
                $data[] = $profile->profile_id;
              }
            }
          }
        }
      }

      // For linked customer profile entities, do the same.

      if ($return_type === 'linked') {

        if (empty($options['requery_all'])) {
          if ($this->settings['import_ids_case_sensitive']) {
            $src_ids = array_map('strtoupper', $src_ids);
          }

          // Preload all profiles at once; this extra code block is probably
          // worth the speedup. See next block for comments.
          $ids = array();
          foreach ($this->context['import']['linked_entities'] as $i => $linked_info) {
            if ($linked_info['type'] === 'commerce_customer_profile') {
              if (isset($this->entityIds)) {
                $linked_bin = "$linked_info[type]:$linked_info[bundle]";
                foreach ($src_ids as $src_id) {
                  // It's possible that the cache doesn't exist, since we don't
                  // do negative caching.
                  if (isset($this->entityIds[$linked_bin][$src_id])) {
                    $ids = array_merge($ids, $this->entityIds[$linked_bin][$src_id]);
                  }
                }
              }
              else {
                foreach ($data as $main_eid => $linked_data) {
                  $ids = array_merge($ids, $linked_data[$i]);
                }
              }
            }
          }
          if ($ids) {
            entity_load('commerce_customer_profile', $ids);
          }
        }

        // Check if any profiles are updatable, and if they are not, return them
        // afterwards. This could theoretically be done by altering the query in
        // the parent, but that's not very easy and makes the code less
        // compartimentalized. So we'll live with the extra separate queries
        // (made from code below) to check individual profiles, for now. (The
        // entity_load()s are not so bad because the caller will likely want to
        // load them too.)
        foreach ($this->context['import']['linked_entities'] as $i => $linked_info) {
          if ($linked_info['type'] === 'commerce_customer_profile') {

            if (isset($this->entityIds)) {
              // Remove the unwanted values from the cache. $data can be left
              // alone; it contains only data for the 'main' cache bin.
              $linked_bin = "$linked_info[type]:$linked_info[bundle]";

              // Deduce which items we need.
              if (!empty($options['requery_all'])) {
                $src_ids = array_keys($this->entityIds[$linked_bin]);
              }

              foreach ($src_ids as $src_id) {
                if (!empty($this->entityIds[$linked_bin][$src_id])) {
                  // Recreate IDs, don't just unset values because we want to keep
                  // 'normal' 0-based keys just to be sure.
                  $ids = array();
                  foreach (entity_load('commerce_customer_profile', $this->entityIds[$linked_bin][$src_id]) as $profile) {
                    if (commerce_order_commerce_customer_profile_can_delete($profile)) {
                      $ids[] = $profile->profile_id;
                    }
                  }
                  $this->entityIds[$linked_bin][$src_id] = $ids;
                }
              }

            }
            else {
              // Remove the unwanted values from $data.
              foreach ($data as $main_eid => $linked_data) {
                if (!empty($linked_data[$i])) {
                  $ids = array();
                  foreach (entity_load('commerce_customer_profile', $linked_data[$i]) as $profile) {
                    if (commerce_order_commerce_customer_profile_can_delete($profile)) {
                      $ids[] = $profile->profile_id;
                    }
                  }
                  $data[$main_eid][$i] = $ids;
                }
              }
            }
          }
        }
      }
    }

    return $data;
  }

}
