<?php
/**
 * @file
 * Contains DrunkinsCommerceStockUpdateJob.
 */

/**
 * Class DrunkinsCommerceStockUpdateJob
 *
 * Can update stock data (and/or a numeric field that behaves like it), into
 * commerce_stock and/or any FieldAPI field.
 * Only updates stock values inside existing entities; does not create new ones.
 *
 * This class uses two things from DrunkinsEntityImportJob:
 * - resolveEntityIds() (a subset of its functionality);
 * - cleanupEntitiesAction() (for the actions; query overridden in this class).
 * There is loads of functionality from the parent (entity) and grandparent
 * (table) importer that is not needed here and it might be a time waster to
 * instantiate this class with multiple bloated ancestors, many times.
 * Rewriting it to get around that, is not worth the effort, though... if the
 * job runner takes care that the class is not instantiated for every item.
 */
class DrunkinsCommerceStockUpdateJob extends DrunkinsEntityImportJob {

  /* This job is capable of adding extra items after a job has already started.
   * (That is: until we add 'capabilities' to a job definition, we should add
   * 'can_override_fetch' and 'can_add_items_while_running' keys into the
   * queue definitions of the corresponding jobs. @todo add 'capabilities' to
   * this class explicitly at some point.) The items can be in the form of:
   * - a 2-element array where the keys are '*action' and 'sku'/'*entity_id';
   *   *actions supported are
   *   - 'cleanup' and 'zero': stock values of the item will be reset (later;
   *     the item gets queued for processing like any other.)
   *   - 'update': stock values will be fetched and updated, one by one (later;
   *     the item gets queued for processing like any other.) The item's second
   *     value must have key 'sku' and a fetcher class must be configured.
   *   - 'update_immediate_fetch': fetches stock for the item immediately; the
   *     resulting item gets queued for processing. Same requirements as for
   *     'update'; the number of items should be small since every item will
   *     prompt a separate fetch operation (just like 'update', but in the same
   *     start() call). This is only a hint; unless this job has
   *     'can_add_items_while_running' == "pass_through_start" capability we
   *     cannot be 100% sure the item will get passed through start() (and if
   *     it's not, it will get treated the same as 'update'). We should not set
   *     that capability value though, because that has a chance of making
   *     drunkins_enqueue_items() calls fail.
   * - an element with keys 'sku' and 'stock';
   * - an element with keys corresponding to the input field names.
   * In the latter two cases, the structure of data should be valid for the
   * specific job settings (i.e. 'import_field_stock' etc). The last case is
   * dangerous if a 'warehouse field' exists and if the capability
   * 'can_add_items_while_running' is not set to "pass_through_start" (which
   * has its own risks), because if a single unformatted item ends up in the
   * queue, stock from other warehouses can end up being set to 0. This triggers
   * a warning in processItem().
   *
   * Settings in $this->settings, used by this class:
   * - job_id:              defined in parent
   * - pkey:                defined in fetcher
   * - fetcher_limit:       defined in fetcher
   * - fetcher_single_item: defined in fetcher
   *
   * - import_entity_type:         defined in DrunkinsEntityImportJob.
   * - import_new_bundle:          defined in DrunkinsEntityImportJob.
   *     Only necessary if we want to set stock for not-encountered items to 0;
   *     so it's a bit of a misnomer here. Only used if
   *     import_cleanup_all_bundles is not set.
   * - import_cleanup_all_bundles: defined in DrunkinsEntityImportJob.
   * - import_dst_pkey*:           defined in DrunkinsEntityImportJob.
   * - import_ids_resolve_fast*:   defined in DrunkinsEntityImportJob.
   * - import_ids_case_sensitive*: defined in DrunkinsEntityImportJob.
   *   * used in resolveEntityIds().
   *
   * (Explicitly NOT used: import_cache_entity_ids. See TODO about precaching
   * in processItems() though; maybe we should change this.)
   *
   * - import_field_stock (string):
   *   Field containing stock value in the source items.
   * - import_field_warehouse (string):
   *   Field containing warehouse values. If you have only one source value for
   *   stock per item, this can be left empty.
   *   Note that only one stock value is processed for each combination of
   *   source item/warehouse; other ones are ignored. If there are multiple rows
   *   per item/warehouse in your source data set, you'll likely need to add a
   *   filter to your query, to get only the stock values you need. (Or
   *   change the query / the code in start() to aggregate amounts.)
   *
   * - import_dst_stock (string):
   *   Field containing stock value in the destination items. If the 'warehouse
   *   field' is defined, this may be left empty - if it's nonempty, this field
   *   will get the total of all per-warehouse values.
   * - import_dst_warehouse (array):
   *   Fields containing per-warehouse stock values in the destination items.
   *     key = any value inside import_field_warehouse in the remote data set.
   *           (Values for warehouses not defined as keys in
   *           import_dst_warehouse are ignored!)
   *     value = destination field for that particular warehouse. (At this
   *             moment, multiple-value fields are not supported.)
   *
   * - import_round_stock (bool): if TRUE, round value and cast to integer.
   *
   * - opt_cleanup (string/array): defined in parent, except there is an extra
   *   value: "zero" sets all configured stock fields (from both import_dst_*
   *   settings) to zero, for entities not encountered in the data set.
   *   Value TRUE is interpreted to be "zero".
   */

  /**
   * {@inheritdoc}
   */
  public function __construct(array $settings) {

    if (isset($settings['opt_list']) && $settings['opt_list'] === TRUE) {
      // We have two different options for listing items.
      $settings['opt_list'] = array(
        'raw'  => 'Raw data set',
        'used' => 'Data used for updating'
      );
    }

    // Set default values.
    if (empty($settings['import_entity_type'])) {
      $settings['import_entity_type'] = 'commerce_product';
    }
    if ($settings['import_entity_type'] === 'commerce_product') {
      // We'll fill some details that are sensible for 99% of stores, so
      // child classes / info hooks don't need to care about them.
      if (empty($settings['import_new_bundle'])) {
        $settings['import_new_bundle'] = 'product';
      }
      if (empty($settings['import_dst_pkey'])) {
        $settings['import_dst_pkey'] = 'sku';
      }
      // Fill default stock destination field, only when per-warehouse field not
      // filled and we're have a 'regular commerce_stock' configuration.
      if (empty($settings['import_dst_stock'])
          && empty($settings['import_dst_warehouse'])
          && module_exists('commerce_stock')) {
        $settings['import_dst_stock'] = 'commerce_stock';
      }
    }

    parent::__construct($settings);

    // In case a subclass sets 'opt_list', we filter away arguments which
    // are useless in the 'display' URL.
    $this->display_arguments_filter = array_merge($this->display_arguments_filter, array(
      'import_dst_stock',
      'import_dst_warehouse',
      'stock_reset_unseen',
    ));
  }

  /**
   * {@inheritdoc}
   * We're not using $context at the moment but will keep it for later / for
   * not deviating from the parent's method definition.
   */
  protected function checkSettings(array $context, $quiet_fast = FALSE) {
    $ok = TRUE;
    foreach (array('pkey', 'entity_type', 'dst_pkey', 'field_stock') as $name) {
      $setting = $name === 'pkey' ? $name : "import_$name";
      if (empty($this->settings[$setting])) {
        if ($ok = $quiet_fast) {
          return FALSE;
        }
        $this->log('@setting setting not configured!', array('@setting' => $setting), WATCHDOG_ERROR);
      }
    }

    if (empty($this->settings['import_dst_warehouse'])) {
      // Stock must be configured if warehouse isn't.
      if (empty($this->settings['import_dst_stock'])) {
        if ($ok = $quiet_fast) {
          return FALSE;
        }
        $this->log('@setting setting not configured!', array('@setting' => 'import_dst_stock'), WATCHDOG_ERROR);
      }
    }
    else {
      // Warehouse dest must be array, source must be set too.
      if (!is_array($this->settings['import_dst_warehouse'])) {
        if ($ok = $quiet_fast) {
          return FALSE;
        }
        $this->log('@setting setting is not an array!', array('@setting' => 'import_dst_warehouse'), WATCHDOG_ERROR);
      }
      if (empty($this->settings['import_field_warehouse'])) {
        if ($ok = $quiet_fast) {
          return FALSE;
        }
        $this->log('@setting setting not configured!', array('@setting' => 'import_field_warehouse'), WATCHDOG_ERROR);
      }
    }

    return $ok;
  }

  /**
   * {@inheritdoc}
   */
  public function start(array &$context) {

    // This is the last place where we can do checks on strangenesses that will
    // make processItem() not do anything.
    // (Better give errors once here than <items> times later.)
    $ok = $this->checkSettings($context);

    // We still continue fetching if we need to just display things (and have
    // the necessary settings).
    if (!$ok
        && (empty($this->settings['list_format'])
            || empty($this->settings['pkey'])
            || empty($this->settings['import_field_stock']))) {
      throw new \Exception("Improper job configuration prevents it from running. Errors have been logged.");
    }

    // Add all keys we're using in the context array. Keep in mind start() can
    // be run multiple times.
    if (!isset($context['updated'])) {
      $context = array_merge($context,
        array(
          // We always set import_ids unlike our parent classes. Maybe we
          // should change that, if we implement 'import_cache_entity_ids'?
          'import_ids'   => array(),
          'updated'      => 0,
          'skipped'      => 0,
          'notpresent'   => 0,
          'error'        => 0,
          'updated_to_0' => 0,
        )
      );
    }

    if (!empty($context['drunkins_override_fetch'])) {
      // We assume
      // - the runner is not buggy so this is always an array;
      // - $this->settings contains no 'modifiers' that influence items fetched,
      //   except for the ones we know about (which are the ones used in this
      //   class, plus 'opt_fetcher_timestamp' which is used in Drunkins'
      //   base fetcher and will ignore the rolling timestamp when turned off).
      $items = $fetched_items = $unformatted_items = array();
      // All items can be added, either as-is or reformatted by formatItems,
      // except for 'update_immediate_fetch'.
      foreach ($context['drunkins_override_fetch'] as $item) {

        if (isset($item['*action']) && count($item) == 2) {

          // Check: 'update(...)' action must have 'sku' in the other element.
          // Other actions may have '*entity_id' or 'sku' in the other element.
          if (!isset($item['sku'])
              && (!isset($item['*entity_id']) || $item['*action'] === 'update' || $item['*action'] === 'update_immediate_fetch')) {
            $this->log("Item which was passed through context with *action == @action has invalid format (no required sku/*entity_id value) and will be ignored.", array('@action' => $item['*action']), WATCHDOG_ERROR, FALSE);
            continue;
          }
          if ($item['*action'] === 'update_immediate_fetch') {
            // Set SKU for current fetch + preprocess operation; cancel other
            // known filters.
            $this->settings['fetcher_single_item'] = $item['sku'];
            unset($this->settings['opt_fetcher_timestamp']);
            // If a previous fetch returned more than (arbitrary) 5 items, we
            // assume it was unfiltered so we can reuse the fetched data set.
            if (count($fetched_items) < 5) {
              $fetched_items = $this->callFetcherMethod('start', NULL, $context);
              if (!isset($fetched_items)) {
                throw new RuntimeException('No fetcher class defined or start() method implemented.');
              }
            }
            $items = array_merge($items, $this->formatItems($fetched_items));
          }
          else {
            // Add '*action' item as-is.
            $items[] = $item;
          }
        }
        elseif (isset($fetched_item['sku']) && isset($fetched_item['stock'])) {
          // This is the format as we expect it. Add as-is.
          $items[] = $item;
        }
        else {
          // This should be preprocessed into an item containing sku/stock. Undo
          // any filtering first. We should bunch all unformatted items together
          // though, in case we need to combine several (e.g. warehouses) into
          // one stock element.
          $unformatted_items[] = $item;
        }
      }
      if ($unformatted_items) {
        // Now format all items into however many stock/sku items come out.
        unset($this->settings['fetcher_single_item']);
        unset($this->settings['opt_fetcher_timestamp']);
        $items = array_merge($items, $this->formatItems($unformatted_items));
      }
    }
    else {
      // Regular single fetch.
      $items = $this->callFetcherMethod('start', NULL, $context);
      if (!isset($items)) {
        throw new RuntimeException('No fetcher class defined or start() method implemented.');
      }

      // 'list_format' can be set to "raw" to return an unmodified remote data
      // set; this is not fit for processItem().
      if (empty($items) || isset($this->settings['list_format']) && $this->settings['list_format'] === 'raw') {
        // Empty data set or raw format requested. Don't cache.
        unset($this->settings['cache_items']);
      }
      else {
        $items = $this->formatItems($items);
      }
    }

    return $items;
  }

  /**
   * Reformat items into a desired structure if needed.
   *
   * @param array $fetched_items
   *
   * @return array
   */
  protected function formatItems(array $fetched_items) {
    // Construct an array of items, in a structure as expected by processitem().
    // (The structure depends on import_field_warehouse being filled - not on
    // import_dst_warehouse.)
    $items = array();
    $i = 0;
    foreach ($fetched_items as $fetched_item) {
      $i++;
      if (empty($fetched_item[$this->settings['pkey']])) {
        $this->log('Source-id value for item #@nr is empty; skipping.',
          array('@nr' => $i));
      }
      // Implement 'single item' filter.
      elseif (empty($this->settings['fetcher_single_item'])
              || $fetched_item[$this->settings['pkey']] == $this->settings['fetcher_single_item']) {

        $sku = $fetched_item[$this->settings['pkey']];
        $stock = $fetched_item[$this->settings['import_field_stock']];
        if (!empty($this->settings['import_round_stock'])) {
          $stock = (int) round($stock);
        }
        if (!is_numeric($stock)) {
          $this->log('Stock value for item @sku (@stock) is not a number; skipping.',
            array('@sku' => $sku, '@stock' => $stock));
        }
        else {
          if (empty($this->settings['import_field_warehouse'])) {
            // We have what we need: an array with sku & stock values.
            // But since we want to prevent superfluous data being moved around in
            // queue tables (and who knows what fields are defined by the connector)
            // let's make an array with only sku + stock fields. Hardcoded keys.
            // The keys will not be used by the caller; we use them to warn about
            // duplicate data rows.
            if (isset($items[$sku])) {
              $this->log('Source-id value for item #@nr (@sku) already encountered in earlier item; skipping this item.',
                array('@nr' => $i, '@sku' => $sku), WATCHDOG_WARNING);
            }
            else {
              $items[$sku] = array('sku' => $sku,
                                   'stock'  => $stock >= 0 ? $stock: 0);
            }
          }
          else {
            // One item can have stock values in several warehouses, which
            // we have as separate rows. Build stock _array_ in one item.
            $warehouse = $fetched_item[$this->settings['import_field_warehouse']];
            if (empty($warehouse)) {
              $this->log('Warehouse value for item @sku is empty; skipping.',
                array('@sku' => $sku));
            }
            elseif (isset($items[$sku]['stock'][$warehouse])) {
              $this->log('Source-id/warehouse combination for item #@nr (@sku/@warehouse) already encountered in earlier item; skipping this item.',
                array('@nr' => $i, '@sku' => $sku, '@warehouse' => $warehouse), WATCHDOG_WARNING);
            }
            else {
              if (!isset($items[$sku])) {
                $items[$sku] = array('sku' => $sku);
              }
              if (!empty($this->settings['list_format'])) {
                // For display: make flat array.
                $items[$sku][$warehouse] = $stock >= 0 ? $stock: 0;
              }
              else {
                $items[$sku]['stock'][$warehouse] = $stock >= 0 ? $stock: 0;
              }
            }
          }
        }
      }

      // Implement 'limit' filter.
      if (!empty($this->settings['fetcher_limit']) && $this->settings['fetcher_limit'] > 0
          && count($items) >= $this->settings['fetcher_limit']) {
        break;
      }
    }
    // Now, $items may or may not have alphanumeric keys. That's OK; the
    // runner should explicitly ignore/discard those.

    return $items;
  }

  /**
   * {@inheritdoc}
   */
  public function processItem($item, array &$context) {

    if (!$this->checkSettings($context, TRUE)) {
      // We can't do anything. We've warned in start(), so exit silently.
      // If this happens, the total reported items in finish() will be less than
      // the number of fetched items.
      return;
    }

    // If a special '(cleanup) action' key is the only thing present besides
    // the source ID, we want to perform the mentioned actions instead of the
    // 'normal' stock update.
    if (isset($item['*action']) && count($item) == 2) {

      // Check what kind of ID we have to operate on.
      if (isset($item['*entity_id'])) {
        // We have no source ID; destination entity already resolved.
        $import_ids = is_array($item['*entity_id']) ? $item['*entity_id'] : array($item['*entity_id']);
      }
      elseif (isset($item['sku'])) {

        // The 'update' action performs an action on the SKU, not the entity ID,
        // so we do that here, not in processEntities(): fetch/process the item.
        // The 'update_immediate_fetch' action should not reach this point but
        // we can't be 100% sure it was passed through start, so also support it
        // here.
        if ($item['*action'] === 'update' || $item['*action'] === 'update_immediate_fetch') {
          $this->settings['fetcher_single_item'] = $item['sku'];
          unset($this->settings['opt_fetcher_timestamp']);
          // Judgment call: since we're already in the middle of the process, we
          // won't give the fetcher the chance to alter context. This may
          // prevent bugs, at the cost of not being able to do customizations.
          $temp_context = array();
          try {
            $fetched_items = $this->callFetcherMethod('start', NULL, $temp_context);
            if (!isset($fetched_items)) {
              throw new RuntimeException('No fetcher class defined or start() method implemented.');
            }
          }
          catch (\Exception $e) {
            $this->log("Exception thrown while trying to get stock for SKU @sku: @message", array('@sku' => $item['sku'], '@message' => $e->getMessage()), WATCHDOG_ERROR);
            return;
          }
          if ($fetched_items) {
            // It's highly likely that we'll have only one item - at least after
            // preprocessing. We could have received multiple lines for multiple
            // warehouses though - so preprocess things first so (probably) one
            // item comes out and then process 'everything' by recursive call.
            foreach ($this->formatItems($fetched_items) as $item) {
              $this->processItem($item, $context);
            }
          }
          else {
            $context['skipped']++;
            if (!empty($this->settings['import_cleanup']) && !empty($this->settings['opt_cleanup'])) {
              // If we clean up stock for products that are not in the data set
              // on finish(), then we should also perform the same action on
              // this product if we didn't find it.
              $entity_ids = $this->resolveEntityIds(array($item['sku']));
              if ($entity_ids) {
                $this->context =& $context;
                $this->processEntities(array('cleanup'), $entity_ids);
              }
            }
          }
          return;
        }

        // Resolve destination from source.
        $import_ids = $this->resolveEntityIds(array($item['sku']));
      }
      else {
        // Skip item.
        $import_ids = array();
      }

      // Construct key to account for action in the finish() message. It
      // should be converted from array('action') to 'action' if possible.
      $action_key = $item['*action'];
      if (is_array($item['*action'])) {
        if (count($item['*action']) == 1) {
          $action_key = reset($item['*action']);
          if (!is_numeric(key($item['*action']))) {
            $action_key = $item['*action'];
          }
        }
        if (is_array($action_key)) {
          // We need some string representation.
          $action_key = json_encode($action_key);
        }
      }

      // If no mapped entity exists, skip the action silently. (This is good
      // for 'cleanup'.)
      if ($import_ids) {

        $this->context =& $context;
        $this->processEntities(
          is_string($item['*action']) ? array($item['*action']) : $item['*action'],
          $import_ids
        );

        // Accounting is done in the same way, regardless whether this is an
        // item with a source ID or only an entity ID. In the former case, we
        // add only 1 even if multiple items were cleaned up. There's no real
        // thought behind this yet; it may need to be refined later.
        //@todo we don't have support for "only an entity ID" yet
        if (!isset($context['actions'][$action_key])) {
          $context['actions'][$action_key] = 0;
        }
        $context['actions'][$action_key]++;
      }
      else {
        if (!isset($context['actions_skipped'][$action_key])) {
          $context['actions_skipped'][$action_key] = 0;
        }
        $context['actions_skipped'][$action_key]++;
      }
    }

    else {
      // Process 'regular item':

      if (!isset($item['sku']) || !isset($item['stock'])) {
        // This should apparently still be preprocessed into an item containing
        // sku/stock. We allow this here because there is a chance the item was
        // not run through start(). It's dangerous to wait until this phase
        // though, because we are now really processing input items one by one.
        // So we cannot e.g. reformat several items for several warehouses into
        // one stock item; if this item contains stock for one warehouse, stock
        // in others will be set to 0.
        // Undo any filtering first.
        unset($this->settings['fetcher_single_item']);
        unset($this->settings['opt_fetcher_timestamp']);
        $items = $this->formatItems(array($item));
        // As stated above, some things are not supported (like 'warehouse').
        if (!$items) {
          // formatItems() logged something but let's do an extra one with higher severity.
          $this->log('Item that was (probably) queued from an external source, was skipped because of unknown/illegal format.', array(), WATCHDOG_WARNING);
          $context['skipped']++;
          return;
        }
        elseif (count($items) > 1) {
          $this->log('Item that was (probably) queued from an external source, was reformatted into @count items. The code cannot handle this and should be adjusted. Skipping everything except the first reformatted item.', array('@count' => count($items), WATCHDOG_ERROR));
        }
        elseif (!isset($item['sku']) || !isset($item['stock'])) {
          $this->log('Item that was (probably) queued from an external source, does not have sku+stock properties after reformatting. Skipping.', array('@count' => count($items), WATCHDOG_ERROR));
          return;
        }
        $item = reset($items);
        if (!empty($this->settings['import_field_warehouse'])) {
          // The quickest way to get to the warehouse value is to look in
          // array_keys(), which should hold one single value only.
          $this->log("Single item for single warehouse @sku/@warehouse was queued is processed in isolation. This may result in other warehouses' stock values being reset to 0.", array('@sku' => $item['sku'], '@warehouse' => implode('/', array_keys($item['stock']))), WATCHDOG_WARNING);
        }
      }

      // A note: we rewrote the base class to call resolveEntityIds with a new
      // return type "entities" at this point, rather than first query IDs and
      // later on do a separate loadEntities() call. We have not checked
      // whether this code could also benefit from that. It likely won't make a
      // difference; see note at resolveEntityIds().
      $import_ids = $this->resolveEntityIds(array($item['sku']));

      if ($import_ids) {
        // Remember encountered entities for finish().
        // Note resolveEntityIds does not cache things by itself, since we did
        // not set $this->entityIds. We won't use that caching because:
        // - we never re-query the same ID again anyway;
        // - we need a simpler structure in finish (a flat array, not a two-
        //   dimensional array keyed by source ID)
        // - minor: the called code inside resolveEntityIds needs to do more
        //   work to cache the less-simple structure, and then convert to the
        //   simple structure for its return value.
        // A child class could set $this->context to activate caching, but then
        // needs to rewrite the use of import_ids in finish().
        //@todo querying and caching things here does not make much sense (if we
        // don't requery on finish), but caching things and only querying once on
        // start() absolutely does, if you process full imports.
        // (In which case we don't need to fill import_ids). Make a setting for that.
        foreach ($import_ids as $id) {
          // This is faster than an array_merge, because $import_ids is small.
          $context['import_ids'][] = $id;
        }

        $entities = $this->loadEntities($import_ids);
        if ($entities) {
          foreach ($entities as $entity) {
            $product_wrapper = entity_metadata_wrapper($this->settings['import_entity_type'], $entity);

            // This code assumes there are _always_ field values to update, and
            // _optionally_ a dedicated commerce_stock value.

            // We'll check current values before overwriting. It's more code
            // intensive but saves having to save the product unnecessarily.
            $changed = FALSE;

            if (is_array($item['stock'])) {

              // Update values for multiple warehouses into all configured
              // fields. If we have no value, reset to 0.
              if (isset($this->settings['import_dst_warehouse'])) {
                foreach ($this->settings['import_dst_warehouse'] as $warehouse => $dst) {
                  $stock = empty($item['stock'][$warehouse]) ? 0 : $item['stock'][$warehouse];
                  if (isset($product_wrapper->$dst) &&
                      $product_wrapper->$dst->value() != $stock) {
                    $product_wrapper->$dst->set($stock);
                    $changed = TRUE;
                  }
                }
              }

              // Update total stock. If there are stock values in $item whose
              // keys are not in 'dst_warehouse', they will still be counted
              // in the total, while not being updated into a warehouse field.
              if ($this->settings['import_dst_stock']) {
                $dst = $this->settings['import_dst_stock'];
                if (isset($product_wrapper->$dst) &&
                    $product_wrapper->$dst->value() != array_sum($item['stock'])) {
                  $product_wrapper->$dst->set( array_sum($item['stock']) );
                  $changed = TRUE;
                }
              }
            }

            else {
              // Update single commerce stock value. We know dst_stock is set.
              $dst = $this->settings['import_dst_stock'];
              if (isset($product_wrapper->$dst) &&
                  $product_wrapper->$dst->value() != $item['stock']) {
                $product_wrapper->$dst->set($item['stock']);
                $changed = TRUE;
              }
            }

            if ($changed) {
              if ($this->saveEntity($entity)) {
                $context['updated']++;
              }
              else {
                $context['error']++;
              }
            }
            else {
              $context['skipped']++;
            }
          }
        }
        else {
          $context['error']++;
        }
      }
      else {
        $context['notpresent']++;
      }
    }
  }

  /**
   * Implements DrunkinsJobInterface::finish().
   */
  public function finish(array &$context) {
    // (Until the messages, this is pretty much the same code as the code from
    // DrunkinsSqlTableImportJob, with a different 'opt_cleanup' default.)

    // Call grand-grandparent (fetcher) method. It's very unlikely we'll get a
    // message, but account for it.
    $message = DrunkinsJob::finish($context);
    if ($message) {
      // Just log it separately, until we change our message handling.
      $this->log($message, array(), WATCHDOG_NOTICE);
    }

    // Clean up 'entities' which were not encountered in the data set.
    // (It's a judgment call if it's better doing it after calling the parent/
    // fetcher finish(); this way, the fetcher can still process some more
    // before cleanup - but we hope it never needs a _cleaned_ data set or
    // statistics on what is/will be cleaned up.)
    $cleanup = !empty($this->settings['import_cleanup'])
           && ($context['updated'] || $context['skipped'])
           && !$context['error']
           && $this->callFetcherMethod('hasFullDataSet', FALSE, $context);
    if ($cleanup) {
      // Validate opt_cleanup option.
      if (empty($this->settings['opt_cleanup'])) {
        $this->log("Setting '@setting1' is set while '@setting2' is not! Ignoring.",
          array('@setting1' => 'import_cleanup', '@setting' => 'opt_cleanup'),
          WATCHDOG_WARNING);
      }
      else {
        $this->context =& $context;
        $this->processEntities(array('cleanup'));
        unset($this->context);
      }
    }

    // Return status message.

    // Always mention 'updated', also if 0.
    $context['updated'] += $context['updated_to_0'];
    $message = format_plural($context['updated'], '1 product updated', '@count products updated');
    if ($context['updated_to_0']) {
      $message .= ' (of which ' . $context['updated_to_0'] . ' are now 0)';
    }
    if ($context['skipped']) {
      $message .= ', ' . $context['skipped'] . ' unchanged';
    }
    if ($context['error']) {
      $message .= ', ' . $context['error'] . ' load errors';
    }
    if ($context['notpresent']) {
      $message .= ', ' . $context['notpresent'] . ' not present';
    }

    // Add 'actions' as normal messages.
    // Depending on the job, these items may have been in the original data set
    // and/ or passed back from finish() in a 'restarted' job - so, depending on
    // the job they may not always add up. Let's revisit later if needed.
    if (isset($context['actions'])) {
      foreach ($context['actions'] as $action => $amount) {
        $message .= "; '$action' action performed on " . format_plural($amount, '1 item', '@count items');
      }
    }
    if (isset($context['actions_skipped'])) {
      foreach ($context['actions_skipped'] as $action => $amount) {
        $message .= "; '$action' action skipped for " . format_plural($amount, '1 nonexisting entity', '@count nonexisting entities');
      }
    }
    $message .= '.';

    // Add 'cleanup' message - the populating of this needs to be explicitly
    // specified by specifying 'collect_ids' as a cleanup action. It may be
    // duplicate functionality with 'actions' messages now but we'll look at
    // that later.
    if (!empty($context['import_processed'])) {
      // Guess the best description.
      $cleanedup_desc = 'cleaned up';
      if (is_array($this->settings['opt_cleanup']) ? in_array('delete', $this->settings['opt_cleanup'])
        : $this->settings['opt_cleanup'] === 'delete') {
        $cleanedup_desc = 'deleted';
      }
      elseif ($this->settings['opt_cleanup'] === array('status' => 0)) {
        $cleanedup_desc = isset($this->settings['import_entity_type'])
                          && $this->settings['import_entity_type'] === 'node' ? 'unpublished' : 'deactivated';
      }
      $message .= '. ' . format_plural(count($context['import_processed']), '1 existing item !cleaned_up which was not in the import data'
        , '@count existing items !cleaned_up which were not in the import data', array('!cleaned_up' => $cleanedup_desc));
    }

    return $message;
  }

  /**
   * {@inheritdoc}
   */
  protected function processEntities(array $actions, array $entity_ids = array()) {

    if ($actions == array('cleanup')) {
      // "cleanup" will be expanded to the 'opt_cleanup' setting value.
      if (empty($this->settings['opt_cleanup'])) {
        // The parent would not be called if !$entity_ids and we don't want to
        // waste time preprocessing, so copy the log message here:
        $this->log("Empty '@option' setting value; ignoring cleanup.", array('@option' => 'opt_cleanup'), WATCHDOG_WARNING, FALSE);
        return;
      }

      // TRUE is not a proper value, but for our class it has a known default:
      if ($this->settings['opt_cleanup'] === TRUE) {
        $this->settings['opt_cleanup'] = array('zero');
      }

      if (!$entity_ids) {
        // Pre-determine the list of entities to clean up.

        $type = $this->settings['import_entity_type'];

        $processed_ids = $this->processedEntityIds();
        if (!$processed_ids) {
          $this->log("Cleanup action could not determine processed items; skipping.", array(), WATCHDOG_WARNING);
          return;
        }

        // Gather all fields containing stock values.
        $fields = array();
        if (isset($this->settings['import_dst_warehouse'])
            && is_array($this->settings['import_dst_warehouse'])) {
          $fields = $this->settings['import_dst_warehouse'];
        }
        if (isset($this->settings['import_dst_stock'])) {
          $fields[] = $this->settings['import_dst_stock'];
        }

        $qstub = new EntityFieldQuery();
        $qstub->entityCondition('entity_type', $type);
        if (!empty($this->settings['import_cleanup_all_bundles'])) {
          // Add a condition on the bundle where supposedly all imported
          // entities were created inside of.
          $qstub->entityCondition('bundle', $this->settings['import_new_bundle']);
        }
        //No ->propertyCondition('status', 1); // that would mean that stock of
        //               inactive products is not OK when they are re-activated.

        // Get all the entities that have ANY of the fields non-zero.
        // EFQ has no 'OR', so use multiple queries.
        $entity_ids = array();
        foreach ($fields as $field) {
          $query = clone $qstub;
          $qres = $query->entityCondition('entity_id', $processed_ids, 'NOT IN')
            ->fieldCondition($field, 'value', 0, '!=')
            ->execute();
          if ($qres) {
            // Remember entities to clean up. Add them to $processed_ids to
            // prevent duplicates.
            $entity_ids = array_merge($entity_ids, array_keys($qres[$type]));
            $processed_ids = array_merge($processed_ids, array_keys($qres[$type]));
          }
        }
      }
    }

    if ($entity_ids) {
      parent::processEntities($actions, $entity_ids);
    }
  }

  /**
   * {@inheritdoc}
   */
  protected function processEntitiesAction($action, array $entity_ids = array()) {

    if ($action === 'zero') {
      // We don't perform our actions on an empty list of entities. Default list
      // should be passed by processEntities().
      if ($entity_ids) {
        // Gather all fields containing stock values.
        $fields = array();
        if (isset($this->settings['import_dst_warehouse'])
            && is_array($this->settings['import_dst_warehouse'])) {
          $fields = $this->settings['import_dst_warehouse'];
        }
        if (isset($this->settings['import_dst_stock'])) {
          $fields[] = $this->settings['import_dst_stock'];
        }

        // Update all fields in all entities to 0.
        // @todo check number of entities, and do in batches (or requeue)
        foreach ($this->loadEntities($entity_ids) as $entity) {
          $p_wrapper = entity_metadata_wrapper($this->settings['import_entity_type'], $entity);

          foreach ($fields as $field) {
            $p_wrapper->$field->set(0);
          }
          $this->saveEntity($entity);
          $this->context['updated_to_0']++;
        }
      }
    }
    else {
      parent::processEntitiesAction($action, $entity_ids);
    }
  }

  /**
   * {@inheritdoc}
   */
  protected function processedEntityIds() {
    // We already have the data we need.
    // @todo see @todo in processItem though, about making this configurable.
    return $this->context['import_ids'];
  }
}
