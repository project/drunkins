<?php
/**
 * @file
 * Contains DrunkinsCommerceProductImportJob.
 */

//@todo use an Exception type with args/tokens/translation context, in various places.

/**
 * Class DrunkinsCommerceProductImportJob
 *
 * Extends import functionality to import data into a commerce_product + display
 * node combination.
 */
class DrunkinsCommerceProductImportJob extends DrunkinsLinkedEntityImportJob {

  /* Settings in $this->settings, used by this class:
   *
   * - all settings mentioned in DrunkinsLinkedEntityImportJob
   *
   * - import_product_new_stock_update_job: if you have a separate job for
   *   updating stock and you want to update the stock for new products shortly
   *   after they are created, then fill this with the job definition (i.e. the
   *   key used in a hook_queue_info() definition). The job must be quite
   *   specific in order to work with this: it must have
   *   'can_add_items_while_running' and 'can_override_fetch' capabilities;
   *   additionally, see comments at saveEntity().
   * - import_product_new_stock_update_job_runner_settings: an array of settings
   *   to pass to the job runner when stock updates are queued.
   */

  /**
   * {@inheritdoc}
   */
  public function __construct(array $settings = array()) {
    /* Note about 'opt_cleanup:1' setting: it has no default value and does not
     * inherit from  'opt_cleanup'. So if we only set opt_cleanup, only the
     * the products will be cleaned up. (There's a chance someone forgets to set
     * 'opt_cleanup:1', but they will see that soon enough in their shop, when
     * products stay visible but become un-purchasable.)
     */

    if (empty($settings['import_entity_type'])) {
      $settings['import_entity_type'] = 'commerce_product';
    }
    if (empty($settings['import_entity_type:1'])) {
      $settings['import_entity_type:1'] = 'node';
    }

    if ($settings['import_entity_type'] === 'commerce_product') {
      // We'll fill some details that are sensible for 99% of stores, so child
      // classes / info hooks don't need to care about them.
      if (empty($settings['import_dst_pkey']) && empty($settings['pkey'])) {
        // Fill import_dst_pkey only if pkey is not set too, because we assume
        // one can be derived from the other through the mapping. (Checking the
        // mapping takes too much time inside a constructor. If they can't be
        // derived, the parent class refuses to run, so it's a good assumption.)
        $settings['import_dst_pkey'] = 'sku';
      }
      // These are 'default' values in Commerce Kickstart:
      if (empty($settings['import_new_bundle'])) {
        $settings['import_new_bundle'] = 'product';
      }
      if (empty($settings['import_main_entity_ref:1'])) {
        $settings['import_main_entity_ref:1'] = 'field_product';
      }
      if (empty($settings['import_new_bundle:1'])) {
        $settings['import_new_bundle:1'] = 'product_display';
      }
    }

    parent::__construct($settings);

  }

  /**
   * {@inheritdoc}
   */
  protected function processEntities(array $actions, array $entity_ids = array()) {

    if ($actions == array('cleanup') && !empty($this->settings['opt_cleanup'])
        && !empty($this->settings['opt_cleanup:1'])) {
      // Clean up the display nodes first.

      if (!$entity_ids) {
        // Copy of parent code:
        // Pre-determine the list of products to clean up. (Which we need here,
        // and we'll pass them on so the parent does not requery.)

        $ids = $this->processedEntityIds();
        if (!$ids) {
          $this->log("Cleanup action could not determine processed items; skipping.", array(), WATCHDOG_WARNING);
          return;
        }
        $entity_type = $this->settings['import_entity_type'];

        $query = new EntityFieldQuery();
        $query->entityCondition('entity_type', $entity_type);
        if (!empty($this->settings['import_cleanup_all_bundles'])) {
          // Add a condition on the bundle where supposedly all imported
          // entities were created inside of.
          $query->entityCondition('bundle', $this->settings['import_new_bundle']);
        }
        $res = $query
          ->entityCondition('entity_id', $ids, 'NOT IN')
          ->propertyCondition('status', 1)
          ->execute();
        if (!isset($res[$entity_type])) {
          return;
        }
        // Pass $entity_ids to the parent so the parent does not execute the same
        // query again.
        $entity_ids = array_keys($res[$entity_type]);
      }

      //@todo implement caching for pid -> nid mapping which could be filled
      //       beforehand? It's not cool if this query needs to be run for every
      //       individual product ID that gets unpublished on processItem()
      //       (fed by either finish() or custom start())

      // Find all published nodes referencing these products, taking into
      // account that a product reference may be multi-value; a node only gets
      // unpublished if no referenced products which are not in $entity_ids, are
      // active.
      // PLEASE NOTE: this means that if it's possible that there are published
      // display nodes in your site with all-deactivated products, and you want
      // to not unpublish those nodes unexpectedly... then $entity_ids should
      // never contain any products which are already inactive before calling
      // this method. Doing this won't affect the products but MAY affect nodes!
      $product_ref = $this->settings['import_main_entity_ref:1'];
      $nids = db_query("SELECT n.nid FROM {node} n
INNER JOIN {field_data_$product_ref} unpub ON n.nid = unpub.entity_id AND unpub.entity_type = 'node' AND unpub.deleted = 0
 AND unpub.{$product_ref}_product_id IN (:pids)
INNER JOIN {field_data_$product_ref} nf ON n.nid = nf.entity_id AND nf.entity_type = 'node' AND nf.deleted = 0
INNER JOIN {commerce_product} cp ON nf.{$product_ref}_product_id = cp.product_id
WHERE n.status = 1
GROUP BY n.nid
HAVING max(CASE nf.{$product_ref}_product_id WHEN unpub.{$product_ref}_product_id THEN 0 ELSE cp.status END) = 0"
        , array(':pids' => $entity_ids))->fetchCol();
      // Note: if you ever try to abstract this class enough so that the main
      // 'product' entity can also be of type "node": things mostly go OK but
      // above is where you get an issue with data being overwritten.

      if ($nids) {
        $this->switchLinkedContext(1);
        parent::processEntities($actions, $nids);
        $this->switchLinkedContext(0);
      }
    }

    parent::processEntities($actions, $entity_ids);
  }

  /**
   * {@inheritdoc}
   */
  protected function executeResolveEntityIdsQuery(array $src_ids, $entity_type, array $options) {

    // Can't think of a use case for a subclass that doesn't set a mapping,
    // but hey, let's keep it generic: check settings.
    $dest_field = isset($this->context['import_dst_pkey']) ? $this->context['import_dst_pkey']
            : (isset($this->settings['import_dst_pkey']) ? $this->settings['import_dst_pkey'] : '');

    // Generalized checks on query_type even though the caller realistically
    // only uses it to store 'linked'...
    $return_type = isset($options['query_type']) ? $options['query_type']
      : (isset($options['return_type']) ? $options['return_type'] : '');

    if ($return_type === 'linked' || $dest_field) {
      // The parent can take care of this.
      return parent::executeResolveEntityIdsQuery($src_ids, $entity_type, $options);
    }

    // We need to resolve linked-entity IDs without using import_dst_pkey;
    // either because the 'primary-in-current-context' entity does not have
    // src_id values (i.e. the import_dst_pkey field is in another entity
    // type) or because $entity_type != 'primary-in-current-context' type.
    //
    // This does not commonly happen but some code in subclasses may need it
    // to do e.g. extra processing on finish().
    // The below has in the past been created for some commerce-product
    // import jobs, and assumes the src_id is the product SKU.

    // @todo: needs to be re-tested when code starts using it again. Use of settings vs origContextSettings should probably be checked.


    $data = array();

    if (!$entity_type) {
      $entity_type = $this->settings['import_entity_type'];
    }
    switch ($entity_type) {
      case 'commerce_product':

        // We don't have a 'portable' way implemented here, though we could.

        if ($return_type === 'keyed' || $options['cache_bin']) {
          // array( Source => array( Dest ) )

          if ($options['cache_bin'] && isset($this->entityIds)) {
            // Append results directly to cache; construct return value later.
            $data =& $this->entityIds[$options['cache_bin']];
          }

          $result = db_query("SELECT sku, product_id FROM {commerce_product} WHERE sku IN (:skus)",
            array(':skus' => $src_ids));
          foreach ($result as $row) {
            // Treat source IDs as case insensitive by default.
            $src_id = !$this->settings['import_ids_case_sensitive'] ?
              strtoupper($row->sku) : $row->sku;

            if (!isset($data[$src_id])) {
              $data[$src_id] = array();
            }
            $data[$src_id][] = $row->product_id;
          }
        }
        else {
          // array( Dest )  (0-based numeric keys)
          $data = db_query("SELECT product_id FROM {commerce_product} WHERE sku IN (:skus)",
            array(':skus' => $src_ids))->fetchCol();
        }
        break;

      case 'node':

        // We don't have a 'portable' way implemented here because we don't
        // have a fieldCondition() to directly query for SKUs.

        $query = db_select('field_data_' . $this->settings['import_main_entity_ref:1'], 'nf');
        $query->condition('nf.entity_type', 'node');
        if (!empty($options['bundle'])) {
          $query->condition('bundle', $options['bundle']);
        }
        $query->innerJoin('commerce_product', 'p',
          'nf.' . $this->settings['import_main_entity_ref:1']
          . '_product_id = p.product_id');

        if (count($src_ids) == 1) {
          $query->condition('p.sku', reset($src_ids));
        }
        else {
          $query->condition('p.sku', $src_ids, 'IN');
        }

        if ($return_type === 'keyed' || $options['cache_bin']) {
          // array( Source => array( Dest ) )

          if ($options['cache_bin'] && isset($this->entityIds)) {
            // Append results directly to cache; construct return value later.
            $data =& $this->entityIds[$options['cache_bin']];
          }

          // array( Source => array( Dest ) )
          $query->addField('p', 'sku');
          $query->addField('nf', 'entity_id', 'nid');
          $result = $query->execute();
          foreach ($result as $row) {
            // Treat source IDs as case insensitive by default.
            $src_id = !$this->settings['import_ids_case_sensitive'] ?
              strtoupper($row->sku) : $row->sku;

            if (!isset($data[$src_id])) {
              $data[$src_id] = array();
            }
            $data[$src_id][] = $row->nid;
          }
        }
        else {
          // array( Dest )  (0-based numeric keys)
          $query->addField('nf', 'entity_id', 'nid');
          $data = $query->execute()->fetchCol();
        }
        break;

      default:
        throw new \Exception("resolveEntityIds was called for destination entity of type $entity_type, but no functionality has been defined for this.");
    }

    return $data;
  }

  /**
   * {@inheritdoc}
   * This method replaces the parent; it adds some custom logic for product
   * display nodes.
   */
  protected function updateEntity($main_entity = NULL, array $linked_ids = array()) {

    $main_entity_id = $return = DrunkinsEntityImportJob::updateEntity($main_entity);

    // Handle linked entities, unless there was an error.
    // If an existing entity wasn't updated, still check/update/create linked.
    if ($main_entity_id === 0 && $main_entity) {
      $main_entity_id = $this->getEntityId($main_entity);
    }

    if (isset($linked_ids[0])) {
      // Mark 'need to create new entities' for each 'linked' type that has no
      // existing entity yet.
      $i = 0;
      while (!empty($this->settings['import_entity_type:' . ++$i])) {
        if (!isset($linked_ids[$i])) {
          $linked_ids[$i] = array();
        }
      }
      unset($linked_ids[0]);
    }

    if ($main_entity_id) {
      foreach ($linked_ids as $linked_index => $entity_ids) {

        $this->switchLinkedContext($linked_index);

        // Possible optimization: preloading entities per entity type instead
        // of per 'linked_type', by grouping them and calling loadEntities()
        // beforehand. Meh.
        $linked_entities = $entity_ids ? $this->loadEntities($entity_ids) : array();
        // If entities fail to load, we won't log an error; probably internal
        // code will. If no entities were loaded here, we create a new one, to
        // prevent situations where invalid data in e.g. the 'main-reference
        // field' table is never noticed & fixed.)
        if (!empty($linked_entities)) {

          // Update existing linked-entity/ies.
          if ($this->context['import_field_mapping']) {
            // If field mapping is set for them, then call updateEntity. The
            // mapping is responsible for updating status/title if necessary.
            foreach ($linked_entities as $linked_entity) {
              $updated_dest_id = DrunkinsEntityImportJob::updateEntity($linked_entity);
              if ($updated_dest_id) {
                // Return a value indicating 'entity was updated' even if the
                // main entity was not (meaning if $return was still 0 here).
                $return = $main_entity_id;
              }
            }
          }
          elseif ($this->settings['import_entity_type'] === 'node') {
            // Display Node specific logic #1:

            // If field mapping is not set for node, we assume nodes hold
            // nothing of interest except title, status and product reference.
            // If title changed, we save node here. Otherwise:
            // If no status change, also do not change node.
            // If status changed to 'unpublished', we'd need to check if nodes
            // reference other products too. Let finish() take care of things.
            // If status changed to 'published': make sure all referencing
            // nodes are published.
            foreach ($linked_entities as $linked_entity) {
              if ($main_entity->title != $linked_entity->title ||
                  ($main_entity->status && !$linked_entity->status)) {

                $linked_entity->title = $main_entity->title;
                $linked_entity->status = $main_entity->status;
                node_save($linked_entity);
              }
            }
          }
        }
        // If no existing linked-entities, create a new one.
        elseif (empty($this->settings['import_new_bundle'])
                || empty($this->settings['import_main_entity_ref'])) {
          // This can't happen really - we've already checked properties.
          $this->log('Not creating linked entity for @id, because required settings not present.', array('@id' => $main_entity_id), WATCHDOG_ERROR);
        }
        else {

          // Create entity and set reference link to main entity.
          $linked_entity = $this->initEntity($main_entity_id);

          if ($this->settings['import_entity_type'] === 'node') {
            // Display Node specific logic #2: set title and status.
            if (!$main_entity) {
              // This is a lot of processing for something which we're >99.9%
              // sure could be solved with one commerce_product_load()...
              $this->switchLinkedContext(0);
              $main_entities = $this->loadEntities(array($main_entity_id));
              $main_entity = reset($main_entities);
              $this->switchLinkedContext($linked_index);
            }
            if (!empty($main_entity->title)) {
              $linked_entity->title = $main_entity->title;
            }
            $linked_entity->status = $main_entity->status;
          }

          // If mapping set, call updateEntity on this initialized node. If
          // not, save it here.
          if (!$this->context['import_field_mapping']
              && !$this->settings['import_default_values']) {

            $this->saveEntity($linked_entity, array(LANGUAGE_NONE => TRUE));
            if ($this->getEntityId($linked_entity)) {
              // Return a value indicating 'entity was updated' even if the main
              // entity was not (meaning if $return was still 0 here).
              $return = $main_entity_id;
            }
          }
          else {
            $updated_dest_id = DrunkinsEntityImportJob::updateEntity($linked_entity);
            if ($updated_dest_id) {
              $return = $main_entity_id;
            }

            if ($this->settings['import_entity_type'] === 'node') {
              // Display Node specific logic #3:
              // If the mapping is set, but does not contain entries for node
              // title/status, these won't get updated. This is a feature. But
              // we'll give a warning anyway.
              // Even though this warning is about updates (not creation),
              // putting it here makes it less intrusive/ever- present; warns
              // the user in advance; and there's a higher chance of seeing it
              // while testing a new import through the UI.
              $map = array();
              if (!isset($this->context['import_field_mapping']['title'])) {
                $map[] = t('Title');
              }
              if (!isset($this->context['import_field_mapping']['status'])) {
                $map[] = t('Status');
              }
              if ($map) {
                $this->log('NOTE: the display node has no source field mapped to its @fields field. This means that, while these have been set initially (from the product), they are not updated when the source data changes!',
                  array('@fields' => implode(' / ', $map)), WATCHDOG_NOTICE, FALSE);
              }
            }
          }
        }
      }

      $this->switchLinkedContext(0);
    }

    return $return;
  }

  /**
   * {@inheritdoc}
   */
  protected function saveEntity($entity, array $changed_parts = array('*' => TRUE)) {

    $old_id = $this->getEntityId($entity);

    $ret = parent::saveEntity($entity, $changed_parts);

    if (!$old_id && $ret && !isset($this->origContextSettings) && !empty($this->settings['import_product_new_stock_update_job'])) {
      // We just saved a new entity. If stock is updated through a separate job
      // and the import does not have stock values, then stock is 0/undefined
      // now and should be updated using the other connector.
      $src_id = isset($this->src_item[$this->context['import_src_pkey']]) ? trim($this->src_item[$this->context['import_src_pkey']]) : '';
      if ($src_id) {
        // It is expected that the job works in a very specific way; basically
        // that it uses DrunkinsCommerceStockUpdateJob as a job class or
        // implements similar '*action = update' functionality, and that we can
        // add custom items to it even when it's already running. This should
        // preferably only queue the item to fetch stock later, because we don't
        // want to perform a potentially blocking (network) operation during
        // this processItem() call.
        $item = array('*action' => 'update', 'sku' => $src_id);
        $runner_settings = (isset($this->settings['import_product_new_stock_update_job_runner_settings']) && is_array($this->settings['import_product_new_stock_update_job_runner_settings'])) ? $this->settings['import_product_new_stock_update_job_runner_settings'] : array();
        // @todo when we have a runner class, stop using a global method.
        drunkins_enqueue_items($this->settings['import_product_new_stock_update_job'], array($item), $runner_settings + array('logging' => $this->settings['logging']));
      }
    }

    return $ret;
  }
}
