<?php
/**
 * @file
 * Contains DrunkinsCommerceStockcombinationJob.
 */

/**
 * Class DrunkinsCommerceStockcombinationJob
 *
 * THIS CLASS IS UNTESTED. It worked before many properties were changed
 * to match changes in the parent, and may or may not be buggy now.
 * It may be possible to rewrite this logic as a fetcher class, which can
 * be inserted in between DrunkinsCommerceStockUpdateJob and another fetcher,
 * and tweak the data set to merge combination stock into the original (parts)
 * stock data, with the help of an extra 'mapping fetcher'. This means some
 * kind-of duplicate code can be removed from this class' start(), and the
 * problem described in finish() won't happen.
 *
 * Until that time, this class will live here for a while as a reminder that
 * once-working code exists for this purpose.
 *
 * --
 *
 * Class providing a job that sets stock values for products that are a
 * combination of other products. With this, stock values can be derived for
 * 'combination products', that either do not have their own stock values in
 * the remote system or that do not exist in the remote system at all.
 * Stock values will be the minimum of the stock values for the individual part
 * products. (These values may or may not all be imported locally already.)
 *
 * The 'main' fetcher should retrieve a mapping from the item nr (sku) of the
 * combination-product to the item nrs of the part-products (multiple rows per
 * combination-product). This can be remote or from Drupal.
 * Stock values for the individual part-products are optionally retrieved either
 * from a second remote 'stock' fetcher, or from Drupal, instead of the 'main'
 * fetcher.
 */
class DrunkinsCommerceStockCombinationJob extends DrunkinsCommerceStockUpdateJob {

  /* Settings in $this->settings, used by this class:
   * - everything in DrunkinsCommerceStockImportJob, see there.
   *
   * The 'main' fetcher will contain combination item nrs in products in the
   * 'pkey' field, and the item numbers for individual parts in the
   * 'import_field_part_sku' field, at least.
   * If the stock values (in 'import_field_stock') can come from the same
   * fetcher, this is best. Otherwise, two things can be done:
   * - Get a second fetcher to return part-products and their stock values (plus
   *   warehouse if needed); this is the same data set as the main fetcher
   *   would return in a normal DrunkinsCommerceStockImportJob.
   *   Set this fetcher class name in 'stock_fetcher_class'.
   * - get the stock values for individual parts from Drupal.
   *   Define 'stock_fetcher_class' as "#drupal".
   *
   * The second thing is OK when you're sure all individual part-products are
   * present in the site as separate entities, with stored stock values.
   * A regular stock import job should ideally have run just before this job.
   * If those entities contain several stock fields, 'import_dst_warehouse' can
   * be set (to an array; only values are important. Its array keys and
   * 'import_field_warehouse' will be overwritten in start().)
   *
   * Defining the fetcher_class as "#drupal" will get items to be updated from
   * Drupal instead; all entities of 'import_entity_type', (optionally)
   * restricted by 'import_new_bundle', will be processed.
   * In this case, 'stock_fetcher_class' must be nonempty, since this is where
   * the stock values must come from.
   *
   * - import_field_part_sku (string)
   *   Field containing the part-product item nrs. Required. Must be present in
   *   the 'stock' fetcher if 'stock_fetcher_class' is set, otherwise in the
   *   'main' fetcher.
   * - stock_fetcher_class (string):
   *   a separate fetcher class which retrieves stock values for the (individual
   *   part) products. Optional; see just above.
   * - import_field_stock_sku (string)
   *   Field containing SKUs in the stock fetcher. Optional; if not filled,
   *   'pkey' is taken.
   *
   * If 'import_field_warehouse' is set, it's assumed to be present in the
   * 'stock' fetcher's data set, not the 'main' fetcher.
   * Note RM: there is also untested support for a 'warehouse' in case of an
   * empty stock_fetcher_class. Stock in multiple warehouses for each part, will
   * produce an array (warehouse => [lowest stock for any part in this
   * warehouse]). But I haven't actually seen how to include a warehouse field!
   */

  /**
   * {@inheritdoc}
   */
  public function __construct(array $settings) {
    parent::__construct($settings);

    // For fetcher_class '#drupal', pkey doesn't need to be defined by
    // the implementing class, because its value doesn't matter. We just set it
    // because it's needed for the code in start() (as a 'field alias' for a
    // local db query) and a parent class may check for its existence.
    if (empty($this->settings['pkey']) || $this->settings['drunkins_fetcher_class'] === '#drupal') {
      $this->settings['pkey'] = 'combo_sku';
    }
    if (!isset($this->settings['import_new_bundle'])) {
      // See finish().
      $this->settings['import_new_bundle'] = '-';
    }
  }

  /**
   * {@inheritdoc}
   */
  protected function checkSettings(array $context, $quiet_fast = FALSE) {
    $ok = parent::checkSettings($context, $quiet_fast);
    if (!$ok) {
      return FALSE;
    }

    // @todo above is not ok - import_field_warehouse can be empty also if
    // import_dst_warehouse is set. Maybe depending on some fetcher class being set.
    // go through this whenever we know we'll use it.

    if (empty($this->settings['drunkins_fetcher_class'])
        || empty($this->settings['import_field_part_sku'])) {
      if ($ok = $quiet_fast) {
        return FALSE;
      }
      $this->log('At least one of fetcher_class / import_field_part_sku settings are not defined; cannot fetch stock', array(), WATCHDOG_ERROR);
    }

    return $ok;
  }

  /**
   * {@inheritdoc}
   */
  public function start(array &$context) {

    // This is the last place where we can do checks on strangenesses that will
    // make processItem() not do anything.
    // (Better give errors once here than <items> times later.)
    $ok = $this->checkSettings($context);

    // We still continue fetching if we need to just display things (and have
    // the necessary settings)
    if (!$ok
        && (empty($this->settings['list_format'])
            || empty($this->settings['pkey'])
            || empty($this->settings['import_field_part_sku'])
            || empty($this->settings['import_field_stock']))) {
      throw new \Exception("Improper job configuration prevents it from running. Errors have been logged.");
    }

    // 'list_format' can be set to "raw" to retrieve an unmodified remote data
    // set; this is not fit for processItem().
    if ($this->settings['drunkins_fetcher_class'] === '#drupal'
        && !empty($this->settings['stock_fetcher_class'])) {
      $main_items = $this->getDrupalItems();
      // $main_items is an array of arrays, or a PDO query result set.
    }
    else {
      $main_items = $this->callFetcherMethod('start', NULL, $context);
      if (!isset($items)) {
        throw new RuntimeException('No fetcher class defined or start() method implemented.');
      }
      // $main_items is an array of arrays.
    }
    // start() is not supposed to ever return a string anymore, so this
    // construction can likely be rewritten.
    if (empty($main_items) || isset($this->settings['list_format']) && $this->settings['list_format'] === 'raw') {
      // Empty data set, error condition or raw format requested. Don't cache.
      unset($this->settings['cache_items']);
      return $main_items;
    }

    $items = array();
    if (!empty($this->settings['stock_fetcher_class'])) {
      // Get stock values for parts.
      if ($this->settings['stock_fetcher_class'] === '#drupal') {
        $stock_items = $this->getDrupalStock($main_items);
      }
      else {
        $stock_items = $this->getFetcherStock();
      }
    }

    // Now construct array of items we need, just like in parent::start().

    $i = 0;
    foreach ($main_items as $main_item) {
      $i++;
      $sku = $main_item[$this->settings['pkey']];
      $part_sku = $main_item[$this->settings['import_field_part_sku']];
      if (empty($this->settings['stock_fetcher_class'])) {
        $stock = $main_item[$this->settings['import_field_stock']];
      }
      if ($sku === '') {
        $this->log('Source-id value for item #@nr is empty; skipping.',
          array('@nr' => $i));
      }
      elseif (empty($part_sku)) {
        $this->log('Part-id value for item @nr (id @sku) is empty; skipping.',
          array('@nr' => $i, '@sku' => $sku));
      }
      elseif (empty($this->settings['stock_fetcher_class']) && !is_numeric($stock)) {
        $this->log('Stock value for item @sku/part @part (@stock) is not a number; skipping.',
          array('@sku' => $sku, '@part' => $part_sku, '@stock' => $stock));
      }
      // Implement 'single item' filter. (for #drupal: part 2/2, and also
      // silently skip setting any stock if the part-product is not present
      // in the website.)
      elseif ((empty($this->settings['stock_fetcher_class'])
               || $this->settings['stock_fetcher_class'] != '#drupal'
               || isset($stock_items[$part_sku]))
              && (empty($this->settings['single_item'])
                  || $sku == $this->settings['single_item'])) {

        // OK, we're done checking inputs...
        // Construct array of objects with settings['pkey'] & stock values, which is
        // what the caller expects. But only include the lowest stock. (The
        // caller will not use array keys; we use them for determining lowest.)

        if (!isset($items[$sku])) {
          // New item (this is the first part we find)

          // Implement 'limit' filter
          if (!empty($this->settings['limit']) &&
              $this->settings['limit'] > 0 &&
              count($items) >= $this->settings['limit']) {
            // Skip this item. (We need to go through the whole loop to see
            // if there are more parts to existing items, though).
            continue;
          }

          if (!empty($this->settings['stock_fetcher_class'])) {
            // #1a: stock coming from another source, in $stock_items, which
            //      has one row per source-id.

            if (empty($this->settings['import_field_warehouse'])) {

              // 'stock' should be single value.  Value in
              // $stock_items may be numeric or NULL (because of the left join).
              $items[$sku] = array(
                'sku' => $sku,
                'stock'  => !empty($stock_items[$part_sku]['s_1']) ?
                  $stock_items[$part_sku]['s_1'] : 0
              );

              if (!empty($this->settings['list_format'])) {
                // Add another field with per-part stock details.
                $items[$sku]['stock_details'] = $part_sku . ': '
                  . (!empty($stock_items[$part_sku]['s_1']) ?
                    $stock_items[$part_sku]['s_1'] : '-');
              }
            }
            else {
              // 'stock' is array, equal to $stock_items row...
              $items[$sku] = array(
                'sku' => $sku,
                'stock'  => isset($stock_items[$part_sku]) ?
                  $stock_items[$part_sku] : array(),
              );
              // ...but without 'sku'. (Which may be set for #drupal)
              unset($items[$sku]['stock']['sku']);

              if (!empty($this->settings['list_format'])) {
                // Add another field with per-part stock details;
                // warehouse names are not displayed (for now?)
                $items[$sku]['stock_details'] = $part_sku . ': '
                  . implode('+', $items[$sku]['stock']);
              }
            }
          }
          else {
            // #2a stock field in this connector, representing the stock of
            // each part product. This means we need to remember the lowest
            // stock value for each part product (possibly per warehouse)
            // as the value for the combination (per warehouse).

            if (empty($this->settings['import_field_warehouse'])) {

              $items[$sku] = array('sku' => $sku,
                                   'stock'  => $stock >= 0 ? $stock: 0);
            }
            else {

              // One item can have stock values in several warehouses, which
              // we have as separate rows. Build stock _array_ in one item.
              $warehouse = $main_item[$this->settings['import_field_warehouse']];
              if (empty($warehouse)) {
                $this->log('Warehouse value for item @sku/part @part (@w) is empty; skipping.',
                  array('@sku' => $sku, '@part' => $part_sku));
              }
              else {
                $items[$sku] = array(
                  'sku' => $sku,
                  'stock' => array(
                    $warehouse => $stock >= 0 ? $stock: 0
                  ));
              }
            }

            if (!empty($this->settings['list_format'])) {
              // Add another field with per-part stock details.
              $items[$sku]['stock_details'] = $part_sku . ': ' . $stock;
            }
          }
        }
        else {
          if (!empty($this->settings['stock_fetcher_class'])) {
            // #1b non-new item; stock in $stock_items

            if (empty($this->settings['import_field_warehouse'])) {
              // Update stock value if it's lower for this 'part'.

              if (empty($stock_items[$part_sku]['s_1'])
                  || $stock_items[$part_sku]['s_1'] < $items[$sku]['stock']) {
                $items[$sku]['stock'] = !empty($stock_items[$part_sku]['s_1']) ?
                  $stock_items[$part_sku]['s_1'] : 0;
              }

              if (!empty($this->settings['list_format'])) {
                // Add another field with per-part stock details.
                $items[$sku]['stock_details'] .= '; ' . $part_sku . ': '
                  . (!empty($stock_items[$part_sku]['s_1']) ?
                    $stock_items[$part_sku]['s_1'] : '-');
              }
            }
            else {
              // Update stock values for 'all warehouses', being:
              // - if $this->stock_dest is an array: all its keys; the other
              //   warehouses won't be updated by processItem() anyway.
              // - otherwise: all keys encountered either in previous
              //   part-items or in this part-item.
              foreach (array_keys((!empty($this->settings['import_field_warehouse'])
                  ? $this->settings['import_field_warehouse']
                  : $items[$sku]['stock'] + $stock_items[$part_sku])
                       ) as $stock_field) {

                if (empty($stock_items[$part_sku][$stock_field]) ||
                    empty($items[$sku]['stock'][$stock_field]) ||
                    $stock_items[$part_sku][$stock_field] < $items[$sku]['stock'][$stock_field]) {

                  $items[$sku]['stock'][$stock_field] =
                    (empty($stock_items[$part_sku][$stock_field]) ||
                     empty($items[$sku]['stock'][$stock_field])) ?
                    0 : $stock_items[$part_sku][$stock_field];
                }
              }

              if (!empty($this->settings['list_format'])) {
                // Add another field with per-part stock details;
                // warehouse names are not displayed (for now?)
                $items[$sku]['stock_details'] .= "; $part_sku: "
                  . implode('+', $items[$sku]['stock']);
              }
            }
          }
          else {
            // #2b non-new item; stock field in this connector.

            if (empty($this->settings['import_field_warehouse'])) {
              // Update stock value if it's lower for this 'part'.
              if ($stock < $items[$sku]['stock']) {
                $items[$sku]['stock'] = $stock >= 0 ? $stock: 0;
              }
            }
            else {
              // Update stock value for this warehouse.
              $warehouse = $main_item[$this->settings['import_field_warehouse']];
              if (empty($warehouse)) {
                $this->log('Warehouse value for item @sku/part @part (@w) is empty; skipping.',
                  array('@sku' => $sku, '@part' => $part_sku));
              }
              elseif (!isset($items[$sku]['stock'][$warehouse]) ||
                      $stock < $items[$sku]['stock'][$warehouse]) {
                $items[$sku]['stock'][$warehouse] = $stock >= 0 ? $stock: 0;
              }
            }

            if (!empty($this->settings['list_format'])) {
              // Add another field with per-part stock details.
              $items[$sku]['stock_details'] .= '; ' . $part_sku . ': ' . $stock;
            }
          }

        } // nonnew item
      } // else (checks were ok)
    } // foreach

    // If used for display, 'flatten' the array.
    if (!empty($this->settings['list_format']) &&
        !empty($this->settings['import_field_warehouse'])) {
      foreach ($items as &$item) {
        $item = array_merge($item, $item['stock']);
        unset($item['stock']);
      }
    }

    // Add all keys we're using in the context array.
    $context = array_merge($context,
      array(
        'import_ids' => array(),
        'updated'    => 0,
        'skipped'    => 0,
        'notpresent' => 0,
        'error'      => 0,
      )
    );

    return $items;
  }

  /**
   * Returns stock values from external connector, as array of arrays.
   * Outer arrays are keyed by item-id (sku); inner arrays are stock values:
   *
   * - if import_field_warehouse is empty: key is 's_1' (for uniform-ness with
   *   drupalStock())
   * - otherwise, keys are warehouse values (just like the parent class)
   */
  protected function getFetcherStock() {

    // Get stock data from separate fetcher.
    $settings = $this->settings;
    $this->settings['drunkins_fetcher_class'] = $this->settings['stock_fetcher_class'];
    // Get stock-fetcher settings, with same keys as fetcher settings.
    // @todo empty out fetcher_* settings first, before merging?
    $this->settings = array_merge($this->settings, $this->settings['stock_fetcher']);
    $fetched_items = $this->callFetcherMethod('start', NULL, $context);
    if (!isset($fetched_items)) {
      throw new RuntimeException('No fetcher class defined or start() method implemented.');
    }
    $this->settings = $settings;

    if (empty($fetched_items) || is_string($fetched_items)) {
      // Empty data set, error condition or raw format requested.
      return $fetched_items;
    }

    $sku_field = !empty($this->settings['import_field_stock_sku'])
      ? $this->settings['import_field_stock_sku'] : $this->settings['pkey'];
    // Get all data into an array with one line per item.
    $items = array();
    $i = 0;
    foreach ($fetched_items as $fetched_item) {
      $i++;
      $sku = $fetched_item[$sku_field];
      if (empty($this->settings['single_item']) || $sku == $this->settings['single_item']) {

        $stock = $fetched_item[$this->settings['import_field_stock']];
        if (!is_numeric($stock)) {
          $this->log('Stock value for item @sku (@stock) is not a number; skipping.',
            array('@sku' => $sku, '@stock' => $stock));
        }
        else {
          if (empty($this->settings['import_field_warehouse'])) {
            if (isset($items[$sku])) {
              $this->log('Part-source-id value for item #@nr (@sku) already encountered in earlier item; skipping this item.',
                array('@nr' => $i, '@sku' => $sku), WATCHDOG_WARNING);
            }
            else {
              $items[$sku] = array('s_1' => $stock >= 0 ? $stock : 0);
            }
          }
          else {
            $warehouse = $fetched_item[$this->settings['import_field_warehouse']];
            if (empty($warehouse)) {
              $this->log('Warehouse value for item @sku is empty; skipping.',
                array('@sku' => $sku));
            }
            elseif (isset($items[$sku]['stock'][$warehouse])) {
              $this->log('Part-source-id/warehouse combination for item #@nr (@sku/@warehouse) already encountered in earlier item; skipping this item.',
                array('@nr' => $i, '@sku' => $sku, '@warehouse' => $warehouse), WATCHDOG_WARNING);
            }
            else {
              $items[$sku][$warehouse] = $stock >= 0 ? $stock : 0;
            }
          }
        }
      }
    }
    return $items;
  }

  /**
   * Returns stock for entities in Drupal as array of arrays. Outer arrays are
   * keyed by item-id (sku); inner arrays are stock values:
   *
   * - if import_dst_warehouse is set, keys are 'sku' and 's_1' - 's_N'.
   *   $this->setting['import_dst_warehouse'] will be changed accordingly
   *   (keys overwritten - and $this->settings['import_field_warehouse'] too).
   *   Changing settings should be OK since we only need this field in start(),
   *   not in processItem().
   * - if import_dst_warehouse is not set: keys are 'sku' and 's_1'.
   *   ('sku' value is not used but would be harder to filter out than keep.)
   */
  protected function getDrupalStock(array $main_items) {

    // Collect SKUs of parts, without duplicates.
    $part_ids = array();
    foreach ($main_items as $main_item) {
      // Implement 'single item' filter (part 1/2; part 2 in caller).
      if (empty($this->settings['single_item']) ||
        $main_item->{$this->settings['pkey']} == $this->settings['single_item']) {

        $part_ids[ $main_item->{$this->settings['import_field_part_sku']} ] = TRUE;
      }
    }

    // Use direct SQL; doing EntityFieldQuery and then loading full products
    // just to get the stock, seems too much overhead - especially on cron.)

    // For the 'source/SKU fields in Drupal'...
    $sku_field = $this->settings['import_dst_pkey'];
    if (empty($sku_field) && $this->settings['import_entity_type'] === 'commerce_product') {
      $sku_field = 'sku';
    }
    $field_info = field_info_field($this->settings['import_field_part_sku']);

    if (!isset($field_info[$sku_field])) {
      // SKU field is (assumed to be) stored in the primary entity table.
      $query = db_select($this->settings['import_entity_type'], 'p');
      $query->condition('p.sku', array_keys($part_ids), 'IN');
      $query->addField('p', 'sku');
      $info = entity_get_info($this->settings['import_entity_type']);
      $join_field = $info['entity keys']['id'];
    }
    else {
      // We don't need the 'entity table'; we can join the 'primary key' table.
      $query = db_select("field_data_$sku_field", 'p');
      $query->condition("p.{$sku_field}_value", array_keys($part_ids), 'IN');
      $query->condition('p.language', LANGUAGE_NONE);
      $query->condition('p.deleted', 0);
      $query->condition('p.delta', 0);
      $query->addField('p', "{$sku_field}_value", 'sku');
      $join_field = 'entity_id';
    }

    // ...look up the stock values (can be multiple)...
    //   (which are commerce_stock_value or a FieldAPI field)
    //   NOTE: assumption we need delta=0 and language=und
    if (empty($this->settings['import_dst_warehouse'])) {

      // We don't need to split things up into different figures for different
      // destination fields, and import_dst_stock holds the total. Get that.
      $stock_field = $this->settings['import_dst_stock'];
      $query->leftJoin("field_data_$stock_field", 's',
        "p.$join_field = s.entity_id AND s.language='" . LANGUAGE_NONE
        . "' AND s.delta=0 AND s.deleted=0");
      $query->addField('s', "{$stock_field}_value", 's_1');
    }
    else {
      if (empty($this->settings['import_dst_warehouse'])) {
        // Take fieldname s_1. (If import_field_warehouse is defined, it will
        // be returned by the caller but not used anywhere.)
        $stock_fields = array('s_1' => $this->settings['import_dst_stock']);
      }
      else {
        // The keys from $this->settings['import_dst_warehouse'] (which
        // represent internal 'warehouse' values in the remote system, in our
        // parent class) will be used as field names in the query we construct.
        // Make sure they're alphanumeric / do not clash with 'sku'.
        $stock_fields = array();
        $i = 0;
        foreach ($this->settings['import_dst_warehouse'] as $stock_field) {
          $stock_fields['s_' . ++$i] = $stock_field;
        }
        // for the caller and processItem():
        $this->settings['import_dst_warehouse'] = $stock_fields;
        // This makes sure an array of stock values is returned by the caller:
        $this->settings['import_field_warehouse'] = 'x';
      }
      foreach ($stock_fields as $alias => $stock_field) {
        $query->leftJoin("field_data_$stock_field", $alias,
          "p.$join_field = $alias.entity_id AND $alias.language='"
          . LANGUAGE_NONE . "' AND $alias.delta=0 AND $alias.deleted=0");
        $query->addField($alias, "{$stock_field}_value", $alias);
      }
    }
    return $query->execute()->fetchAllAssoc('sku', PDO::FETCH_ASSOC);
  }

  /**
   * Return mapping of item nr (sku) of main product -> item nr of product parts
   * assuming all that info is present in Drupal.
   */
  protected function getDrupalItems() {

    // Use direct SQL, as in getDrupalStock().

    $info = entity_get_info($this->settings['import_entity_type']);

    // Add field for item SKU. (With alias: $this->settings['pkey'].)
    $sku_field = $this->settings['import_dst_pkey'];
    if (empty($sku_field) && $this->settings['import_entity_type'] === 'commerce_product') {
      $sku_field = 'sku';
    }
    $sku_field_info = field_info_field($this->settings['import_field_part_sku']);

    if (!isset($sku_field_info[$sku_field])) {
      $query = db_select($this->settings['import_entity_type'], 'p');
      $query->addField('p', $sku_field, $this->settings['pkey']);
      $join_field = $info['entity keys']['id'];;
    }
    else {
      // We don't need the 'entity table'; we can link against the
      // 'primary key' table.
      $query = db_select("field_data_$sku_field", 'p');
      $query->condition('p.language', LANGUAGE_NONE);
      $query->condition('p.deleted', 0);
      $query->condition('p.delta', 0);
      $query->addField('p', "{$sku_field}_value", $this->settings['pkey']);
      $join_field = 'entity_id';
    }
    if (empty($this->settings['import_resolve_all_bundles'])) {
      $query->condition('p.' . $info['entity keys']['bundle'], $this->settings['import_new_bundle']);
    }

    // Add field for part SKUs. (With alias: $this->settings['import_field_part_sku'].)
    // We support the data being in Drupal text fields and product references.
    // Add a left join so we also catch products consisting of 0 parts; the
    // caller does nothing with them except generate a warning.

    $query->leftJoin('field_data_' . $this->settings['import_field_part_sku'], 'pp',
      "p.$join_field = pp.entity_id AND pp.language='"
      . LANGUAGE_NONE . "' AND pp.delta=0 AND pp.deleted=0");
    $field_info = field_info_field($this->settings['import_field_part_sku']);
    if (!isset($field_info['type'])) {
      $this->log('import_field_part_sku setting (%pf) is not a proper Drupal field.',
        array('%pf' => $this->settings['import_field_part_sku']), WATCHDOG_ERROR);
      return array();
    }
    elseif ($field_info['type'] === 'text') {

      $query->addField('s', $this->settings['import_field_part_sku'] . '_value', $this->settings['import_field_part_sku']);
    }
    elseif ($field_info['type'] === 'commerce_product_reference') {

      // Join either the entity table or the field table (see 'Add field for
      // item SKU' above) to the product_id.
      if (!isset($sku_field_info[$sku_field])) {
        $query->leftJoin($this->settings['import_entity_type'], 'ppf',
          'pp.' . $this->settings['import_field_part_sku'] . '_product_id = ppf.' . $join_field);
        $query->addField('ppf', $sku_field, $this->settings['import_field_part_sku']);
      }
      else {
        $query->leftJoin("field_data_$sku_field", 'ppf',
        'pp.' . $this->settings['import_field_part_sku'] . "_product_id = ppf.entity_id AND ppf.language='"
        . LANGUAGE_NONE . "' AND ppf.delta=0 AND pp.deleted=0");
        $query->addField('p', $sku_field . '_value', $this->settings['import_field_part_sku']);
      }
    }
    else {
      $this->log('import_field_part_sku setting (%pf) has unrecognized field type (%type); maybe the code should be extended.',
        array('%pf' => $this->settings['import_field_part_sku'], '%type' => $field_info['type']), WATCHDOG_ERROR);
      return array();
    }

    if ($this->settings['stock_fetcher_class'] === '#drupal') {
      // We need to loop over the query results twice; once in getDrupalStock()
      // and once in the caller. So turn the query result into an array.
      $rows = array();
      foreach ($query->execute() as $row) {
        $rows[] = $row;
      }
      return $rows;
    }
    return $query->execute();
  }

  /**
   * {@inheritdoc}
   */
  public function finish(array &$context) {
    /* By default, no stock is updated to 0 because 'import_new_bundle' is set
     * to -.
     *
     * What should really be done is updating all product combinations which
     * were not encountered in the connector, to 0 here. And then
     * doing the same for _only_ all non-combination products in
     * DrunkinsCommerceStockImportJob::finish() -- but we have no standard way
     * of distinguishing between the two types.
     *
     * This means that, if you run DrunkinsCommerceStockImportJob alongside
     * with this one, the stock of combinations will alternate between 0 (set by
     * DrunkinsCommerceStockImportJob::finish()) and the actual value (set by
     * this method), unless you take special measures.
     * A good way, for instance, would be to give regular products and product
     * combinations separate bundles (Product Types in Commerce Product UI).
     * Or subclass both classes and override the finish() methods.
     */

    // Return status message.

    // Always mention 'updated', also if 0.
    $message = format_plural($context['updated'], '1 product updated', '@count products updated');
    if ($context['skipped']) {
      $message .= ', ' . $context['skipped'] . ' unchanged';
    }
    if ($context['error']) {
      $message .= ', ' . $context['error'] . ' load errors';
    }
    if ($context['notpresent']) {
      $message .= ', ' . $context['notpresent'] . ' not present';
    }
    $message .= '.';
    return $message;
  }
}
