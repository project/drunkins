<?php

/**
 * Contains common functionality for counting results of item processing.
 *
 * The minimum to know about intended usage:
 * - In start(), call initializeCounters(['updated', 'error', 'skipped', ...]).
 *   (Repeated 'initialize' calls are OK and do not reset values.)
 * - In processItem(), call increaseCounter('updated'/'error'/other counter)
 * - In finish(), use the counters to summarize the job's actions in the way
 *   you want to, possibly using getCounters().
 *
 * Summary:
 *
 * This keeps counters/summaries of various types of process outcomes (e.g.
 * updated, skipped/equal, error encountered) in the job context. It doesn't
 * preclude other code from using the same context values / doesn't protect
 * against tampering; this is just a 'convenience' trait, providing a uniform
 * set of methods to call.
 *
 * We make a distinction between two types of 'counters' (where the second type
 * isn't really a counter, but we can turn it into one to save context space):
 * - Simple counters, which will just be increased;
 * - Value stores, which can hold an array of values, often being IDs of the
 *   items to process, which can be used in post processing.
 * Value stores should be used only if needed, because they can bloat the
 * context. To minimize context/code bloat, a job can initialize a certain name
 * to be a counter 'emulating' a value store. (This makes
 * "if/then storeValue()/else increaseCounter()" constructs unnecessary for
 * preventing context bloat; code can just call storeValue(). The value will
 * not actually be stored; only a counter will be increased on each call.)
 *
 * Terminology note: 'counters' is interchangeably used for both "simple
 * counters as distinguished from value stores" and "the combinations of simple
 * counters and value stores"; that could be a bit confusing.
 *
 * It's possible that this piece of logic will be moved from jobs into
 * context classes, if we get those - and the save method will move elsewhere.
 */
trait DrunkinsProcessSummaryTrait {

  /**
   * The 'namespace' in the context where all counter values are stored.
   *
   * It has no setter/getter. This is here mainly so that jobs which have
   * preexisting counters can change it to '' temporarily to retain their old
   * behavior, as long as all their child classes are not using this trait yet.
   *
   * If you change this, do it in the job's constructor to be sure it's set
   * consistently. If you change it to '', note that this has effects on the
   * return value of getCounters(). We hope to phase this property out later.
   *
   * @var string
   */
  protected $processSummaryNamespace = 'process_summary';

  /**
   * Returns the part of the context containing the counters and value stores.
   *
   * WARNING about behavior: please note the second argument if you set
   * $processSummaryNamespace to '' for backward compatibility.
   *
   * As long as $this->processSummaryNamespace exists,
   * - code should always use this function to get the counters are. In the
   *   future (after we have a better 'context class') this method might be
   *   replaced by something else; certainly the second argument will be phased
   *   out.
   * - counters will not be seen by getCounters(, TRUE) and e.g. its caller
   *   saveProcessSummaryCounts(), if $this->processSummaryNamespace is empty.
   *
   * @param array $context
   *   The process context. By default, this array can be returned by-reference
   *   and changed. (We're not guarding of our data, because our data is the
   *   job's data and we're just a collection of convenience methods.)
   * @param bool $only_registered_counters_in_empty_namespace
   *   (Optional) if $this->processSummaryNamespace is set to an empty string,
   *   we need to take care: by default it will return the full context, but
   *   then you don't know which properties are counters and which are not.
   *   If this is set to TRUE, only the 'counters' part will be returned but
   *   this is slower, takes more memory, and the context isn't returned by
   *   'by reference' so changing it will not change the context (if
   *   $this->processSummaryNamespace is empty).
   *
   * @return array
   *   The part of the context containing the counters and value stores.
   *   Individual counters / value stores are not validated to hold the correct
   *   type of value, so a careful caller should still do that.
   */
  protected function &getCounters(array &$context, $only_registered_counters_in_empty_namespace = FALSE) {
    if (empty($this->processSummaryNamespace)) {
      if ($only_registered_counters_in_empty_namespace) {
        // Put this in a variable so we can return it by reference (though we
        // have no other reference to this value, so there's no practical
        // difference with returning it by value in this case).
        $context_slice = array_intersect_key($context, $context['process_summary_meta']['registered_counters']);
        return $context_slice;
      }
      return $context;
    }

    // It's allowed for the whole context itself to not be initialized yet.
    if (!isset($context[$this->processSummaryNamespace])) {
      $context[$this->processSummaryNamespace] = [];
    }
    elseif (!is_array($context[$this->processSummaryNamespace])) {
      // This is not a 'warn and continue' situation; some code messed up in
      // a way that should not be allowed to happen and that should be
      // possible to fix.
      throw new LogicException('The process counter context has an invalid structure.');
    }
    return $context[$this->processSummaryNamespace];
  }

  /**
   * Initializes simple counters.
   *
   * Calling this method isn't strictly required to use counters (by calling
   * increaseCounter()) but:
   * - Warnings will be logged when undefined counters are increased (because
   *   we expect a job wants to know if it is using a 'wrong' counter name)
   *   unless they're suppressed using setUnknownCountersCauseWarnings(FALSE);
   * - getProcessSummaryCount() will return 0 instead of NULL for an
   *   initialized counter that never gets used;
   * - This call can indicate that it's OK to call these counters as if they
   *   are value stores (but that we still don't want to actually store the
   *   values, to save space).
   *
   * 'Initializing' counters repeatedly will not emit an error and will not
   * reinitialize the counters. (We expect a job context to start out
   * completely empty and never have to be reinitialized.) This means the
   * method can be called repeatedly (from e.g. repeated calls to start())
   * without issues.
   *
   * @param string[] $counter_names
   *   Names of counters to initialize.
   * @param bool $emulate_value_store
   *   (Eventually optional) indicator that this counter can 'emulate' a value
   *   store, i.e. requests to store a value will just cause the counter to be
   *   increased, rather than the default behavior of logging a warning that a
   *   value cannot be stored in a counter. Warnings as a result of (past or
   *   future) calls to initializeValueStores() for these counter names are
   *   suppressed.
   * @param array $context
   *   Context. This is the last parameter because we expect to phase it out on
   *   a rewrite, hopefully someday. (So for now, all callers will in practice
   *   have to specify $emulate_value_store; sorry.)
   *
   * @throws \LogicException
   *   A counter was previously initialized as a value store.
   */
  public function initializeSimpleCounters(array $counter_names, $emulate_value_store, array &$context) {
    $counters =& $this->getCounters($context);

    foreach ($counter_names as $counter_name) {
      // If the counter has an unknown type, we don't complain;
      // increaseCounter() will.
      if (!isset($counters[$counter_name])) {
        $counters[$counter_name] = 0;
      }
      elseif (is_array($counters[$counter_name])) {
        // We assume this was initialized as a value store. Log a warning
        // because this is a sign that code should be changed - but it's not
        // critical enough to break off the import. Except... if
        // $emulate_value_store=TRUE, we don't want to log a warning (just like
        // a repeated initialize-call usually doesn't log anything); we want to
        // 'assimilate' the value store silently, throwing away the values. The
        // common use case for this is: a child class can 'turn off' storing
        // lots of values, without the parent class needing to change code.
        // (This cannot be turned back on.)
        if ($emulate_value_store) {
          $counters[$counter_name] = count($counters[$counter_name]);
        } else {
          $this->log("'@name' counter has apparently been initialized as a value store earlier, and cannot be reinitialized as a simple counter.", ['@name' => $counter_name], WATCHDOG_WARNING);
        }
      }
    }

    // Remember the counter names. (We might get rid of this later because so
    // far we only use this when $this->processSummaryNamespace is empty, so
    // 'outside code' should not depend on it. increaseCounter() and
    // storeValue() just check presence and type of value stored in the
    // context, and have no need to doublecheck the registered counter names.)
    $counter_names_flipped = array_flip($counter_names);
    if (!isset($context['process_summary_meta']['registered_counters'])) {
      $context['process_summary_meta']['registered_counters'] = [];
    }
    $context['process_summary_meta']['registered_counters'] += $counter_names_flipped;
    if ($emulate_value_store) {
      // Remember that these counters are OK with being used by storeValue().
      if (!isset($context['process_summary_meta']['simple_counters_emulating_stores'])) {
        $context['process_summary_meta']['simple_counters_emulating_stores'] = [];
      }
      $context['process_summary_meta']['simple_counters_emulating_stores'] += $counter_names_flipped;
    }
  }

  /**
   * Initializes value stores.
   *
   * Calling this method isn't strictly required to use value stores (by
   * calling storeValue()) but:
   * - Warnings will be logged when undefined value stores are used (because
   *   we expect a job wants to know if it is using a 'wrong' store name)
   *   unless they're suppressed using setUnknownCountersCauseWarnings(FALSE);
   * - getProcessSummaryCount() will return 0 instead of NULL for an
   *   initialized counter that never gets used.
   *
   * 'Initializing' value stores repeatedly will not emit an error and will not
   * reinitialize the stores. (We expect a job context to start out completely
   * empty and never have to be reinitialized.) This means the method can be
   * called repeatedly (from e.g. repeated calls to start()) without issues.
   *
   * Note there is no requirement to use these 'value stores' for storing
   * values; code can store values elsewhere in the context without issues.
   * Using these dedicated 'value stores' only adds a little helper function
   * and can summarize/save the count of items at the end of the process, along
   * with the other counters.
   *
   * @param string[] $store_names
   *   Names of value stores to initialize.
   * @param array $context
   *   Context. This is the last parameter because we expect to phase it out on
   *   a rewrite, hopefully someday.
   *
   * @throws \LogicException
   *   A counter was previously initialized as a simple counter.
   */
  protected function initializeValueStores(array $store_names, array &$context) {
    $counters =& $this->getCounters($context);

    foreach ($store_names as $store_name) {
      // If the counter has an unknown type, we don't complain; storeValue()
      // will.
      if (!isset($counters[$store_name])) {
        $counters[$store_name] = [];
      }
      elseif (!is_array($counters[$store_name])
              && !isset($context['process_summary_meta']['simple_counters_emulating_stores'][$store_name])) {
        // We assume this was initialized as a counter before. Log a warning
        // because this is a sign that code should be changed - but it's not
        // critical enough to break off the import. Except skip the warning
        // if this is a counter that 'emulates' a value store.
        $this->log("'@name' counter has apparently been initialized as a simple counter earlier, and cannot be reinitialized as a value store.", ['@name' => $store_name], WATCHDOG_WARNING);
      }
    }

    // Remember the counter names. (We might get rid of this later because we
    // only use this when $this->processSummaryNamespace is empty (so far), so
    // 'outside code' should not depend on it.)
    if (!isset($context['process_summary_meta']['registered_counters'])) {
      $context['process_summary_meta']['registered_counters'] = [];
    }
    $context['process_summary_meta']['registered_counters'] += array_flip($store_names);
  }

  /**
   * Increases a counter.
   *
   * @param string $counter_name
   *   The name of the counter to increase.
   * @param int $amount
   *   (Optional) amount to increase the counter by. (Unfortunately often not
   *   as optional as we'd like it to be, as long as $context still exists...)
   * @param array|null $context
   *   (Optional) context. This parameter may be omitted, for forward
   *   compatibility with a situation where we will likely replace the context
   *   array by another object - but there's a danger in this: the caller must
   *   be sure that $this->context is aliased to the context array, otherwise
   *   the counter cannot be increased and an error will be logged (or the
   *   value will just get lost).
   */
  public function increaseCounter($counter_name, $amount = 1, array &$context = NULL) {
    if (filter_var($amount, FILTER_VALIDATE_INT) === FALSE) {
       $this->log("@value is not an integer; we cannot use it to increase the '@name' counter.", ['@value' => var_export($amount, TRUE), '@name' => $counter_name], WATCHDOG_ERROR);
       return;
    }
    if (!isset($context)) {
      if (!isset($this->context) || !is_array($this->context)) {
        $this->log('increaseCounter() called without $context argument, and context property is also not set in the object. Counter cannot be increased.', [], WATCHDOG_ERROR);
        return;
      }
      $context =& $this->context;
    }

    $counters =& $this->getCounters($context);
    if (!isset($counters[$counter_name])) {
      if ($this->getUnknownCountersCauseWarnings($context)) {
        $this->log("'@name' counter is used without being initialized. (Is the counter name correct? To suppress this warning, either initialize the counter beforehand or actively turn off these kinds of warnings.)", ['@name' => $counter_name], WATCHDOG_WARNING);
      }
      $counters[$counter_name] = 0;
    }
    if (is_int($counters[$counter_name])) {
      $counters[$counter_name] += $amount;
    }
    else {
      $this->log("'@name' counter does not contain an integer; we cannot increase its value.", ['@name' => $counter_name], WATCHDOG_ERROR);
    }
  }

  /**
   * Stores a value to be used later - or just increases a simple counter.
   *
   * @param string $store_name
   *   The name of the store to add values to; this uses the same namespace as
   *   'simple counters'. If this was actually registered explicitly as a
   *   simple counter earlier (for the purpose of not letting the context grow
   *   if it's not needed), the counter will be increased and the value will be
   *   discarded.
   * @param mixed $value
   *   The value to store.
   * @param string|int|null $key
   *   (Optional) key to store the value under. The common thing to do is
   *   to pass null here, in which case the value will be added to the store
   *   with a unique numeric key. If the key is not null and another value was
   *   already stored under this key, it will be overwritten without warning.
   *   NOTE that calling this repeatedly with the same key can influence the
   *   reported count later on, depending on how this store name was
   *   initialized: if it was initialized as a simple counter (disguised as a
   *   value store) instead of a value store, every storeValue() call will
   *   increase the counter - while for normal value stores this does not
   *   happen when calling storeValue() with the same key, because the count is
   *   just the number of values stored.
   * @param array|null $context
   *   (Optional) context. This parameter may be omitted, for forward
   *   compatibility with a situation where we will likely replace the context
   *   array by another object - but there's a danger in this: the caller must
   *   be sure that $this->context is aliased to the context array, otherwise
   *   the counter cannot be increased and an error will be logged (or the
   *   value will just get lost).
   */
  protected function storeValue($store_name, $value, $key = NULL, array &$context = NULL) {
    if (!isset($context)) {
      if (!isset($this->context) || !is_array($this->context)) {
        $this->log('storeValue() called without $context argument, and context property is also not set in the object. Value cannot be stored.', [], WATCHDOG_ERROR);
        return;
      }
      $context =& $this->context;
    }

    $counters =& $this->getCounters($context);
    if (!isset($counters[$store_name])) {
      if ($this->getUnknownCountersCauseWarnings($context)) {
        $this->log("'@name' value store is used without being initialized. (Is the name correct? To suppress this warning, either initialize the value store beforehand or actively turn off these kinds of warnings.)", ['@name' => $store_name], WATCHDOG_WARNING);
      }
      // PHP strictly doesn't need this, but...
      $counters[$store_name] = [];
    }
    if (is_array($counters[$store_name])) {
      if (isset($key)) {
        // Overwrite any existing value silently.
        $counters[$store_name][$key] = $value;
      }
      else {
        $context[$store_name][] = $value;
      }
    }
    elseif (isset($context['process_summary_meta']['simple_counters_emulating_stores'][$store_name])) {
      $this->increaseCounter($store_name, 1, $context);
    }
    else {
      $this->log("'@name' counter does not contain an array; we cannot store a value.", ['@name' => $store_name], WATCHDOG_ERROR);
    }
  }

  /**
   * Get the count for a certain recorded result / counter.
   *
   * @param string $result_type
   *   The 'result type', a.k.a. the name of a counter (or value store).
   * @param array $context
   *   The process context.
   *
   * @return int|null
   *   The count, which is always an integer, unless this counter was never
   *   initialized or increased, then NULL.
   */
  public function getProcessSummaryCount($result_type, $context) {
    $counters = $this->getCounters($context);
    if (isset($counters[$result_type])) {
      return is_array($counters[$result_type]) ? count($counters[$result_type]) : $counters[$result_type];
    }
    return NULL;
  }

  /**
   * Saves all counter values, and number of items in value stores, in a table.
   *
   * This data could be used later to summarize activity for repeat job runs.
   *
   * PLEASE NOTE: This method will likely move elsewhere in due time. (Not to
   * the new context object, but to some management class. The main reason it's
   * here is we depend on $this->processSummaryNamespace for the time being; if
   * that is phased out, we can just move this into drunkins.run.inc or so.)
   *
   * Number of items in a value store could be inexact if the job stored values
   * under the same key repeatedly; see storeValue() comments.
   *
   * This method is not guaranteed to keep operating (though we'll keep it from
   * throwing exceptions; it might just stop recording data somehow).
   * Specifically, we don't know yet whether some assumptions / behavior will
   * stay true in the future: at the moment, it is assumed that this method is
   * called exactly when the job is finished, and inserts a row of data into
   * the drunkins_finished_process table as well as the counters.
   *
   * @param int $process_id
   *   Process ID as registered in the database.
   * @param bool $save_zeroes
   *   (Eventually optional) indicator that values of zero should also be saved.
   * @param array $context
   *   Context. This is the last parameter because we expect to phase it out on
   *   a rewrite, hopefully someday.
   */
  public function saveProcessSummaryCounts($process_id, $save_zeroes, array $context) {
    // We may be called while the job isn't even implementing counters.
    // (Basically because it's not the job's responsibility to decide whether
    // to save anything - it's the module's, usually configured by the
    // site administrator. That's also why it's public.)
    $counters = $this->getCounters($context, TRUE);
    if ($counters) {
      foreach ($counters as $counter_name => $counter) {
        if ($counter || $save_zeroes) {
          db_insert('drunkins_process_counter')
            ->fields([
              'process_id' => $process_id,
              'name' => $counter_name,
              'count' => is_array($counter) ? count($counter) : $counter,
            ])
            ->execute();
        }
      }
    }
  }

  /**
   * Specifies whether warnings should be emitted for using unknown counters.
   *
   * This starts out TRUE, so set this to FALSE if you don't want warnings.
   * There is a side effect as long as we still have
   * $this->processSummaryNamespace: if it's empty, un-initialized counters
   * (that would cause a warning) are not seen by getCounters(, TRUE).
   *
   * @param bool $warn
   *   If TRUE, a warning will be emitted if a caller tries to access a counter
   *   or value store that was not initialized. If FALSE, this won't happen.
   * @param array $context
   *   Context. This is the last parameter because we expect to phase it out on
   *   a rewrite, hopefully someday.
   */
  protected function setUnknownCountersCauseWarnings($warn, array &$context) {
    // We store this in context because it needs to stay the same throughout
    // the process (and we can't set it in settings - it's on the job class to
    // set this, not on outside config).
    $context['process_summary_meta']['suppress_warnings_for_unknown_counters'] = !$warn;
  }

  /**
   * Specifies whether warnings are emitted for using unknown counters.
   *
   * @param array $context
   *   Context. This is the last parameter because we expect to phase it out on
   *   a rewrite, hopefully someday.
   *
   * @return bool
   *   If TRUE, a warning will be emitted if a caller tries to access a counter
   *   or value store that was not initialized. If FALSE, this won't happen.
   */
  protected function getUnknownCountersCauseWarnings(array $context) {
    // The default (i.e. context value is not set) should return TRUE, which is
    // why we've flipped the meaning of the context value and return empty().
    return empty($context['process_summary_meta']['suppress_warnings_for_unknown_counters']);
  }

}
